﻿namespace SamboyLauncherNew
{
    public struct Version
    {
        public int versionMajor;
        public int versionMinor;
        public int versionPatch;

        public override string ToString()
        {
            return $"{versionMajor}.{versionMinor}.{versionPatch}";
        }

        public void FromString(string verString)
        {
            string[] split = verString.Split('.');
            versionMajor = int.Parse(split[0]);
            versionMinor = split.Length > 1 ? int.Parse(split[1]) : 0;
            versionPatch = split.Length > 2 ? int.Parse(split[2]) : 0;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Version))
            {
                return false;
            }

            var version = (Version)obj;
            return versionMajor == version.versionMajor &&
                   versionMinor == version.versionMinor &&
                   versionPatch == version.versionPatch;
        }

        public override int GetHashCode()
        {
            var hashCode = -181834713;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + versionMajor.GetHashCode();
            hashCode = hashCode * -1521134295 + versionMinor.GetHashCode();
            hashCode = hashCode * -1521134295 + versionPatch.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Version thisO, object two)
        {
            if (two is Version crTwo)
            {
                return crTwo.ToString() == thisO.ToString();
            }

            return false;
        }

        public static bool operator !=(Version thisO, object two)
        {
            return !(thisO == two);
        }
    }
}