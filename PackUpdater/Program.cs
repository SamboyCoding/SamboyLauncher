﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Version = SamboyLauncherNew.Version;

namespace PackUpdater
{
    class Program
    {
        private const bool _DEBUG = false;

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int cmdShow);

        static void Main(string[] args)
        {
            Process p = Process.GetCurrentProcess();
            ShowWindow(p.MainWindowHandle, 3);
            string path = args.Length == 0 ? null : args[0];
            if (path == null || path.Trim().Length == 0)
            {
                Console.WriteLine("[Fatal] No launcher path provided.");
                ExitWithDelay(1, 3000);
                return;
            }

            Console.WriteLine("----------------------------------------------------------------------------------------------------");
            Console.WriteLine(@"
  _                            _                 _    _           _       _
 | |                          | |               | |  | |         | |     | |
 | |     __ _ _   _ _ __   ___| |__   ___ _ __  | |  | |_ __   __| | __ _| |_ ___ _ __
 | |    / _  | | | |  _ \ / __|  _ \ / _ \  __| | |  | |  _ \ / _  |/ _  | __/ _ \  __|
 | |___| (_| | |_| | | | | (__| | | |  __/ |    | |__| | |_) | (_| | (_| | ||  __/ |
 |______\__,_|\__,_|_| |_|\___|_| |_|\___|_|     \____/| .__/ \__,_|\__,_|\__\___|_|
                                                       | |
                                                       |_|                             ");
            Console.WriteLine("----------------------------------------------------------------------------------------------------\r\n\r\n");

            if(_DEBUG)
                Console.WriteLine("[Debug] Raw Path: " + path);

            path = Path.GetFullPath(path);

            Console.WriteLine("[Info] Launcher is located at " + path);
            if (!File.Exists(path))
            {
                Console.WriteLine("[Fatal] Wait... no it isn't...");
                ExitWithDelay(1, 5000);
                return;
            }

            Console.WriteLine("[Info] Terminating Launcher Process...");

            string processName = path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal) + 1).Replace(".exe", "");

            if (_DEBUG)
                Console.WriteLine("[Debug] Looking for all processes by name: " + processName);
            foreach (Process process in Process.GetProcessesByName(processName))
            {
                if (process.Id == 4 || process.Id == 0) continue; //System/Broken Process
                Console.WriteLine("[Info] Terminating process " + process.Id);
                process.Kill();
            }

            string launcherDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "SamboyLauncher_New");

            Console.WriteLine("[Info] Launcher installation is located at " + launcherDirectory);

            string latestFile = Path.Combine(launcherDirectory, "launcher.latest");

            Console.WriteLine("[Info] Downloading https://launcher.samboycoding.me/api/launcher/updates -> " + latestFile);
            if (!(bool) DownloadFile("https://launcher.samboycoding.me/api/launcher/updates", latestFile))
            {
                Console.WriteLine("[Fatal] Failed to get https://launcher.samboycoding.me/api/launcher/updates. Exiting in 5 seconds");
                ExitWithDelay(1, 5000);
                return;
            }

            string previousFile = Path.Combine(launcherDirectory, "launcher.current");
            bool shouldUpdate = false;

            if (!File.Exists(previousFile))
            {
                shouldUpdate = true;
                Console.WriteLine("[Warning] No current version file (" + previousFile + ") found, assuming current launcher version is too old");
            }
            else
            {
                string oldVers = File.ReadAllText(previousFile);
                Version old = new Version();
                old.FromString(oldVers);

                string newVers = File.ReadAllText(latestFile);
                Version newV = new Version();
                newV.FromString(newVers);

                if (newV.versionMajor > old.versionMajor || (newV.versionMajor == old.versionMajor && newV.versionMinor > old.versionMinor)
                                                         || (newV.versionMajor == old.versionMajor && newV.versionMinor == old.versionMinor &&
                                                             newV.versionPatch > old.versionPatch))
                {
                    Console.WriteLine("[Info] New Version (" + newV + ") available! (Current is " + old + ")");
                    shouldUpdate = true;
                }
                else
                {
                    Console.WriteLine("[Info] No new version available (latest is " + newV + " and current is " + old + ")");
                }
            }

            if (shouldUpdate)
            {
                Console.WriteLine("[Info] Updating launcher from https://launcher.samboycoding.me/download");
                if (!(bool) DownloadFile("https://launcher.samboycoding.me/download", path))
                {
                    Console.WriteLine("[Fatal] Failed to update launcher! Exiting in 5 seconds...");
                    ExitWithDelay(1, 5000);
                    return;
                }

                Console.WriteLine("Updated successfully. Opening updated launcher...");
                Process.Start(new ProcessStartInfo
                {
                    FileName = path,
                    Arguments = "--updated",
                    WorkingDirectory = Path.GetDirectoryName(path)
                });
            }

            Console.WriteLine("[Info] Finished. Exiting in 2 seconds or on user return key hit.");
            ExitWithDelay(0, 2000);
        }

        static void ExitWithDelay(int code, int delay)
        {
            Task.Run(() =>
            {
                Thread.Sleep(delay);
                Environment.Exit(code);
            });
            Console.ReadLine(); //Wait
        }

        private static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            try
            {
                return (HttpWebResponse) req.GetResponse();
            }
            catch (WebException we)
            {
                if (!(we.Response is HttpWebResponse resp))
                    throw;
                return resp;
            }
        }

        /// <summary>
        /// Downloads a file, and either returns the contents, or, if dest is provided, saves it to a file specified by dest.
        /// </summary>
        /// <param name="source">The URL to get the file from</param>
        /// <param name="dest">The destination to save the file to. If null (default) then the contents of the file are returned.</param>
        /// <returns>The contents of the file (or null if the download failed) if dest is null or not provided, or true if the file was saved, or
        /// false if it wasn't.</returns>
        private static object DownloadFile(string source, string dest = null, bool suppress = false)
        {
            HttpWebRequest request = WebRequest.CreateHttp(source);
            //We're chrome 63 now
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";

            try
            {
                if (dest != null)
                {
                    WebClient Client = new WebClient();
                    Client.DownloadFile(source, dest);
                    return true;
                }


                using (HttpWebResponse response = GetResponseNoException(request))
                using (Stream responseStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Console.WriteLine("[Warning] Response code of " + response.StatusCode + " received from " + source + "!");
                        if (_DEBUG)
                            Console.WriteLine(reader.ReadToEnd());
                        return null;
                    }

                    return reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                if (!suppress) Console.WriteLine(e);
                return dest == null ? (object) null : false;
            }
        }
    }
}