﻿using System;
using System.Collections.Generic;

namespace SamboyLauncherNew
{
    public struct Pack
    {
        public uint packId;
        public string packName;
        public string description;

        public Version version;

        public string mcVers;
        public string fmlVers;

        public List<Mod> mods;

        public Uri url;

        public override string ToString()
        {
            return $"Pack{{Name={packName}, version={version}, SourceURL={url}, mcVersion={mcVers}, " +
                   $"forgeVersion={fmlVers}, mods={mods?.Count ?? 0}}}";
        }

        public string GetModsHumanReadable()
        {
            string result = "";
            if (mods != null)
            {
                foreach (Mod m in mods)
                {
                    result += m.ToString();
                    result += "\r\n";
                }
            }

            return result;
        }
    }
}