﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ionic.Zip;
using Newtonsoft.Json.Linq;
using SharpCompress.Compressors.Xz;

namespace SamboyLauncherNew
{
    public static class GameLauncher
    {
        private static MainForm _main;

        [DllImport ("libc")]
        static extern int uname (IntPtr buf);

        static bool IsRunningOnMac ()
        {
            IntPtr buf = IntPtr.Zero;
            try {
                buf = Marshal.AllocHGlobal (8192);
                // This is a hacktastic way of getting sysname from uname ()
                if (uname (buf) == 0) {
                    string os = Marshal.PtrToStringAnsi (buf);
                    if (os == "Darwin")
                        return true;
                }
            }
            catch
            {
                // ignored
            }
            finally {
                if (buf != IntPtr.Zero)
                    Marshal.FreeHGlobal (buf);
            }
            return false;
        }

        private static void Log(object t)
        {
            _main.Log(t);
        }

        private static void SetStatusSafe(string status)
        {
            _main.SetStatusSafe(status);
        }

        private static float GetProgressRaw()
        {
            return _main.GetProgressRaw();
        }

        private static void SetProgressSafe(float prg)
        {
            _main.SetProgressSafe(prg);
        }

        private static object DownloadFile(string source, string dest = null, bool suppress = false)
        {
            return _main.DownloadFile(source, dest, suppress);
        }

        private static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            return MainForm.GetResponseNoException(req);
        }

        //Starts at start, continues to end
        private static void GetAssetsRange(JObject objects, string baseassetspath, int start, int end)
        {
            Log("Thread Spawned, Fetching assets " + start + "-" + (end - 1));
            int pos = 0;

            float increasePer = 15f / objects.Count;
            foreach (var o in objects)
            {
                if (pos++ < start) continue;
                if (pos > end) break;

                SetProgressSafe(GetProgressRaw() + increasePer);

                JObject asset = (JObject) o.Value;
                string hash = asset["hash"].ToString();
                string url = "https://resources.download.minecraft.net/" + hash.Substring(0, 2) + "/" + hash;

                Log("GET Asset " + pos + " from " + url + " => assets/" + o.Key);

                string folder = Path.Combine(baseassetspath, "objects", hash.Substring(0, 2));

                if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

                string localpath = Path.Combine(baseassetspath, "objects", hash.Substring(0, 2), hash);
                if (!File.Exists(localpath))
                {
                    if (!(bool) DownloadFile(url, localpath))
                    {
                        Log("FAILED TO GET ASSET");
                        return;
                    }
                }

                //Handle legacy paths
                string path = Environment.OSVersion.Platform == PlatformID.Win32NT ? o.Key.Replace("/", "\\") : o.Key;

                string legacyPath = Path.Combine(baseassetspath, "virtual", "legacy", path);
                if (!Directory.Exists(Path.GetDirectoryName(legacyPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(legacyPath));

                if (File.Exists(legacyPath)) continue;

                File.Copy(localpath, legacyPath);
            }

            Log("Thread Fetching assets " + start + "-" + (end - 1) + " completed.");
        }

        private static bool GetVanillaVersionManifest(string mcVers, string vanillaVersionFolder)
        {
            SetProgressSafe(5);
            SetStatusSafe("Grabbing Details for Minecraft " + mcVers);

            Log("GET https://launchermeta.mojang.com/mc/game/version_manifest.json");
            object result = DownloadFile("https://launchermeta.mojang.com/mc/game/version_manifest.json");

            if (result == null)
            {
                Log("Failed to get file!");
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                    MessageBox.Show("Failed to get global version manifest! Aborting Launch!");
                return false;
            }

            JObject manifest = JObject.Parse(result.ToString());

            JArray versions = (JArray) manifest["versions"];
            JObject versionInfo = (JObject) versions.FirstOrDefault(v => v["id"].ToString() == mcVers);

            if (versionInfo == null)
            {
                Log("No version in manifest with id of " + mcVers + "!");
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                    MessageBox.Show("Cannot find Minecraft Version " + mcVers +
                                    " in global manifest! Aborting Launch!");
                return false;
            }

            Log("Getting URL for manifest for " + mcVers);
            string url = versionInfo["url"].ToString();

            if (!Directory.Exists(vanillaVersionFolder)) Directory.CreateDirectory(vanillaVersionFolder);

            Log("GET " + url);
            result = DownloadFile(url, Path.Combine(vanillaVersionFolder, "version.json"));

            if ((bool) result) return true;

            Log("Failed to get file!");
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                MessageBox.Show("Failed to get version manifest for MC " + mcVers + "! Aborting Launch!");

            return false;
        }

        private static bool GetVanillaLibraries(List<JObject> libraries, string libsFolder)
        {
            SetStatusSafe("Downloading Libraries...");
            SetProgressSafe(6);

            if (!Directory.Exists(libsFolder))
            {
                Log("MkDir " + libsFolder);
                Directory.CreateDirectory(libsFolder);
            }

            float currPrg = 6;
            float totalProgressPer = 14f / libraries.Count; //This'll get us to 20%;
            foreach (var lib in libraries)
            {
                SetStatusSafe("Downloading " + lib["name"] + "...");
                SetProgressSafe(currPrg += totalProgressPer);
                string url = lib["downloads"]["artifact"]["url"].ToString();

                string relativePath = url.Replace("https://libraries.minecraft.net/", "");
                List<string> pathPieces = new List<string> {libsFolder};
                pathPieces.AddRange(relativePath.Split('/'));
                string path = Path.Combine(pathPieces.ToArray());

                if (File.Exists(path)) continue;

                string folder = Path.Combine(path, "..");

                if (!Directory.Exists(folder))
                {
                    Log("Mkdir " + folder);
                    Directory.CreateDirectory(folder);
                }

                Log("GET " + url + " => " + path);
                DownloadFile(url, path);

                if (!File.Exists(path))
                {
                    Log("Download failed - file does not exist upon download finish!");
                    return false;
                }
            }

            return true;
        }

        private static bool GetNatives(List<JObject> natives, string nativesFolder, string tempFolder, string currentOs, string arch)
        {
            List<JObject> realNatives = new List<JObject>();

            Log("Filtering out natives we really need to get...");

            foreach (var nat in natives)
            {
                if (!nat.ContainsKey("rules"))
                {
                    realNatives.Add(nat);
                    continue;
                }


                foreach (var rule in nat["rules"])
                {
                    if (!((JObject) rule).ContainsKey("os"))
                    {
                        realNatives.Add(nat);
                        continue;
                    }

                    if (rule["os"]["name"].ToString() != currentOs) continue;

                    if (rule["action"].ToString() == "allow")
                    {
                        realNatives.Add(nat);
                    }
                    else
                    {
                        realNatives.Remove(nat);
                    }
                }
            }

            Log("Grabbing URLs...");

            List<string> nativeUrlsToGet = new List<string>();
            int totalSize = 0;

            foreach (var nat in realNatives)
            {
                string classifier = null;
                foreach (var osname in (JObject) nat["natives"])
                {
                    if (osname.Key == currentOs)
                    {
                        classifier = osname.Value.ToString().Replace("${arch}", arch);
                    }
                }

                if (classifier == null && currentOs == "osx")
                {
                    //Falback to linux for OSX
                    foreach (var osname in (JObject) nat["natives"])
                    {
                        if (osname.Key == "linux")
                        {
                            classifier = osname.Value.ToString().Replace("${arch}", arch);
                        }
                    }
                }

                if (classifier == null)
                {
                    Log("Unable to launch. Native " + nat["name"] + $" should be installed on {currentOs}-{arch} but no URL could be found.");
                    if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                        MessageBox.Show(nat["name"] + " TELLS US WE NEED IT BUT DOES NOT CONTAIN A CLASSIFIER FOR US! Abort Launch!");
                    return false;
                }

                totalSize += nat["downloads"]["classifiers"][classifier]["size"].ToObject<int>();
                nativeUrlsToGet.Add(nat["downloads"]["classifiers"][classifier]["url"].ToString());
            }

            float totalProgressPer = 20f / nativeUrlsToGet.Count;
            float currPrg = 20;

            Log("Need to get " + nativeUrlsToGet.Count + " natives totalling " + totalSize + " bytes.");

            if (!Directory.Exists(nativesFolder))
            {
                Log("MkDir " + nativesFolder);
                Directory.CreateDirectory(nativesFolder);
            }

            if (!Directory.Exists(tempFolder))
            {
                Log("MkDir " + tempFolder);
                Directory.CreateDirectory(tempFolder);
            }

            foreach (string natUrl in nativeUrlsToGet)
            {
                string natName = natUrl.Substring(natUrl.LastIndexOf("/", StringComparison.Ordinal) + 1);
                string natPath = Path.Combine(tempFolder, natName);
                SetStatusSafe("Downloading " + natName);
                SetProgressSafe(currPrg += totalProgressPer);

                Log("GET " + natUrl + " => " + natPath);
                DownloadFile(natUrl, natPath);

                if (!File.Exists(natPath))
                {
                    Log("Download failed! Native zip does not exist after download complete!");
                    return false;
                }

                Log("EXTRACT " + natPath + " => " + nativesFolder);

                using (ZipFile zip = ZipFile.Read(natPath))
                {
                    zip.ExtractAll(nativesFolder, ExtractExistingFileAction.OverwriteSilently);
                }

                if (Directory.Exists(Path.Combine(nativesFolder, "META-INF")))
                    Directory.Delete(Path.Combine(nativesFolder, "META-INF"), true);

                File.Delete(natPath); //Cleanup the native
            }

            Log("Finished downloading natives");

            return true;
        }

        public static void LaunchPack(string launcherDirectory, Pack p, MainForm mainForm)
        {
            _main = mainForm;

            /**********************************************************
             * Detect OS And determine + print paths
             *********************************************************/

            Log("\nLaunching pack...");
            Log("Locating paths...");
            SetStatusSafe("Locating Paths...");

            string vanillaVersionFolder =
                Path.Combine(launcherDirectory, "versions", "vanilla", p.mcVers);
            string forgeVersionFolder = Path.Combine(launcherDirectory, "versions", "forge", p.fmlVers);
            string libsFolder = Path.Combine(launcherDirectory, "libraries");
            string tempFolder = Path.Combine(launcherDirectory, "temp");
            string nativesFolder = Path.Combine(vanillaVersionFolder, "natives");
            string url;
            JObject versionManifest = null;
            List<JObject> libraries;

            Log("=========Information=========");
            Log("Base Launcher Dir: " + launcherDirectory);
            Log("Vanilla game installation dir: " + vanillaVersionFolder);
            Log("Forge Installation dir: " + forgeVersionFolder);
            Log("Library Root: " + libsFolder);
            Log("Temporary Working dir: " + tempFolder);
            Log("Natives Root: " + nativesFolder);
            Log("=============================\n");

            Log("Detecting Operating System and Architecture...");

            string currentOs;
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.MacOSX:
                    currentOs = "osx";
                    break;
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    currentOs = "windows";
                    break;
                case PlatformID.Unix:
                    currentOs = "linux";
                    break;
                case PlatformID.Xbox:
                    //NO! WHY! THIS IS NOT SUPPOSED TO BE A THING!
                    Log(
                        "AAAAAAAHHHHHH WE'RE ON AN XBOX?! HOW THE HELL DID WE GET ON AN XBOX?! WHY WOULD YOU NEED A MINECRAFT LAUNCHER ON AN XBOX?!");
                    return;
                default:
                    currentOs = "noop";
                    break;
            }

            if (IsRunningOnMac()) currentOs = "osx";

            string arch = Environment.Is64BitOperatingSystem ? "64" : "32";

            Log("OS is " + currentOs + "-" + arch);

            /**********************************************************
             * Wait for mods to resolve.
             *********************************************************/

            //TODO: Improve this to avoid freezing

            SetStatusSafe("Waiting for mods to be resolved...");
            Log("Waiting for mods to be resolved...");
            SetProgressSafe(1);
            bool ready = false;
            while (!ready)
            {
                if (p.mods.All(m => m.IsResolved()))
                {
                    ready = true;
                }

                if (p.mods.Any(m => m.HasErrored()))
                {
                    if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                        MessageBox.Show(
                            "One or more mods has failed to resolve. Thus, the pack cannot be launched. See the mod list box for " +
                            "details, and consult the pack author.", "Error Launching");
                    return;
                }

                Task.Delay(1000).Wait();
            }

            /**********************************************************
             * Install Vanilla
             *********************************************************/

            string installedVanillaVersionsFile =
                Path.Combine(launcherDirectory, "versions", "vanilla", "installedversions.mani");
            string forgeVerMan = Path.Combine(launcherDirectory, "versions", "forge", "installedversions.mani");

            //If the required vanilla version isn't installed
            if (!(File.Exists(installedVanillaVersionsFile) && File.ReadAllLines(installedVanillaVersionsFile).Contains(p.mcVers)))
            {
                if (!GetVanillaVersionManifest(p.mcVers, vanillaVersionFolder))
                {
                    Log("\n\n-------Installation Step: *Get Vanilla Version Manifest* Failed--------\n");
                    return;
                }

                //Read the manifest
                versionManifest = JObject.Parse(File.ReadAllText(Path.Combine(vanillaVersionFolder, "version.json")));

                Log("Enumerating libraries...");
                libraries = new List<JObject>(versionManifest["libraries"]
                    .Where(lib => !((JObject) lib).ContainsKey("natives"))
                    .Select(lib => (JObject) lib));

                int totalSize = 0;
                libraries.ForEach(lib => totalSize += lib["downloads"]["artifact"]["size"].ToObject<int>());

                Log("Need to download " + totalSize + " bytes of libraries.");

                if (!GetVanillaLibraries(libraries, libsFolder))
                {
                    Log("\n\n-------Installation Step: *Get Vanilla Libraries* Failed--------\n");
                    return;
                }

                SetProgressSafe(21);
                SetStatusSafe("Downloading Natives...");
                Log("Enumerating Natives...");
                List<JObject> natives = new List<JObject>(versionManifest["libraries"]
                    .Where(lib => ((JObject) lib).ContainsKey("natives"))
                    .Select(lib => (JObject) lib));

                if (!GetNatives(natives, nativesFolder, tempFolder, currentOs, arch))
                {
                    Log("\n\n-------Installation Step: *Get Vanilla Natives* Failed--------\n");
                    return;
                }

                SetStatusSafe("Grabbing Minecraft.jar...");
                SetProgressSafe(41);
                Log("Grabbing Game JAR");

                string clientUrl = versionManifest["downloads"]["client"]["url"].ToString();

                Log("GET " + clientUrl);
                DownloadFile(clientUrl, Path.Combine(vanillaVersionFolder, "minecraft.jar"));

                SetProgressSafe(45);
                SetStatusSafe("Getting Assets...");
                Log("Getting Assets Manifest...");

                string assetsManifestUrl = versionManifest["assetIndex"]["url"].ToString();
                Log("GET " + assetsManifestUrl);

                object content = DownloadFile(assetsManifestUrl);

                if (content == null)
                {
                    Log("Failed to get " + assetsManifestUrl);
                    if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                        MessageBox.Show("Failed to get asset list! Aborting Launch!");
                    return;
                }

                string assetManifestDir = Path.Combine(launcherDirectory, "assets", "indexes");

                if (!Directory.Exists(assetManifestDir)) Directory.CreateDirectory(assetManifestDir);

                using (var writer = File.CreateText(Path.Combine(assetManifestDir, p.mcVers + ".json")))
                {
                    writer.Write((string) content);
                    Log("Saved a copy of the assets manifest as " +
                        Path.Combine(assetManifestDir, p.mcVers + ".json"));
                }

                JObject assetManifest = JObject.Parse((string) content);
                int numAssets = ((JObject) assetManifest["objects"]).Count;
                Log(numAssets + " assets to download.");
                Log("Spawning 4 threads to get assets...");

                int perThread = (int) Math.Floor(numAssets / 4f);
                int lastThread = perThread + numAssets % 4;

                Task task1 = Task.Run(() =>
                    GetAssetsRange((JObject) assetManifest["objects"],
                        Path.Combine(launcherDirectory, "assets"), 0, perThread + 1));

                Task task2 = Task.Run(() =>
                    GetAssetsRange((JObject) assetManifest["objects"],
                        Path.Combine(launcherDirectory, "assets"), perThread + 1,
                        (perThread + 1) * 2));

                Task task3 = Task.Run(() =>
                    GetAssetsRange((JObject) assetManifest["objects"],
                        Path.Combine(launcherDirectory, "assets"), (perThread + 1) * 2,
                        (perThread + 1) * 3));

                Task task4 = Task.Run(() =>
                    GetAssetsRange((JObject) assetManifest["objects"],
                        Path.Combine(launcherDirectory, "assets"), (perThread + 1) * 3,
                        (perThread + 1) * 3 + lastThread + 1));

                task1.Wait();
                task2.Wait();
                task3.Wait();
                task4.Wait();

                Log("Got all assets.");

                using (FileStream stream = File.Exists(installedVanillaVersionsFile)
                    ? File.OpenWrite(installedVanillaVersionsFile)
                    : File.Create(installedVanillaVersionsFile))
                using (StreamWriter vvmWriter = new StreamWriter(stream))
                {
                    vvmWriter.BaseStream.Seek(0, SeekOrigin.End);
                    vvmWriter.WriteLine(p.mcVers);
                    Log("Saved installed state of vanilla " + p.mcVers);
                }
            } //End section not done if mc already downloaded

            float percentPer;
            JObject forgeVersionManifest = null;

            if (!string.IsNullOrWhiteSpace(p.fmlVers) && !(File.Exists(forgeVerMan) && File.ReadAllLines(forgeVerMan).Contains(p.fmlVers)))
            {
                Log("Grabbing Forge...");

                SetProgressSafe(61);
                SetStatusSafe("Installing Forge...");

                if (!Directory.Exists(forgeVersionFolder)) Directory.CreateDirectory(forgeVersionFolder);

                string forgeJarURL =
                    $"http://files.minecraftforge.net/maven/net/minecraftforge/forge/{p.mcVers}-{p.fmlVers}-{p.mcVers}/forge-{p.mcVers}-{p.fmlVers}-{p.mcVers}-universal.jar";
                string localPath = Path.Combine(forgeVersionFolder, "forge.jar");

                if (!File.Exists(localPath))
                {
                    Log("GET " + forgeJarURL + " => " + localPath);

                    if (!(bool) DownloadFile(forgeJarURL, localPath, true))
                    {
                        forgeJarURL =
                            $"http://files.minecraftforge.net/maven/net/minecraftforge/forge/{p.mcVers}-{p.fmlVers}/forge-{p.mcVers}-{p.fmlVers}-universal.jar";

                        Log("GET " + forgeJarURL);

                        if (!(bool) DownloadFile(forgeJarURL, localPath))
                        {
                            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                                MessageBox.Show("Failed to get forge jar! Aborting Launch!");
                            Log("Couldn't get forge jar.");
                            return;
                        }
                    }

                    Log("Got Forge Jar.");
                }

                Log("Extracting version info from forge jar...");
                using (ZipFile zip = ZipFile.Read(localPath))
                {
                    foreach (ZipEntry entry in zip.Entries)
                    {
                        if (entry.FileName == "version.json")
                        {
                            entry.Extract(forgeVersionFolder, ExtractExistingFileAction.OverwriteSilently);
                        }
                    }
                }

                Log("Extracted " + forgeVersionFolder + "\\version.json");

                Log("Parsing forge version info...");
                forgeVersionManifest =
                    JObject.Parse(File.ReadAllText(Path.Combine(forgeVersionFolder, "version.json")));

                SetProgressSafe(65);
                SetStatusSafe("Getting forge libraries...");
                Log("Getting forge libraries...");

                int count =
                    ((JArray) forgeVersionManifest["libraries"]).Count(
                        lib => ((JObject) lib).ContainsKey("clientreq") && lib.Value<bool>("clientreq"));

                Log("Need to get " + count + " libraries for forge");

                percentPer = 15f / count;

                foreach (var o in (JArray) forgeVersionManifest["libraries"])
                {
                    JObject lib = (JObject) o;
                    SetProgressSafe(GetProgressRaw() + percentPer);
                    string name = lib["name"].ToString();

                    if (name.Contains("net.minecraftforge:forge")) continue;

                    SetStatusSafe("Getting " + name);

                    string[] split = name.Split(':');

                    string path = split[0].Split('.').Aggregate("", (current, subfolder) => current + subfolder + "/");

                    path += split[1] + "/" + split[2] + "/" + split[1] + "-" + split[2] + ".jar";

                    string localLibPath = Path.Combine(libsFolder,
                        Environment.OSVersion.Platform == PlatformID.Win32NT ? path.Replace("/", "\\") : path);

                    if (!Directory.Exists(Path.GetDirectoryName(localLibPath)))
                        Directory.CreateDirectory(Path.GetDirectoryName(localLibPath));

                    if (lib.ContainsKey("url")) url = lib["url"] + path;
                    else url = "https://libraries.minecraft.net/" + path;


                    Log("GET " + url + " => " + localLibPath);

                    if (!(bool) DownloadFile(url, localLibPath, true))
                    {
                        Log("Failed to get " + name + ", trying packed form...");
                        url = url + ".pack.xz";
                        if (!(bool) DownloadFile(url,
                            Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack.xz")))
                        {
                            Log("Failed to get " + name + " even in packed form.");
                            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                                MessageBox.Show("Failed to get a library for forge! Aborting Launch!");
                            return;
                        }

                        Log("Got packed form. Un-XZ'ing...");

                        try
                        {
                            if (File.Exists(Path.GetFileName(localLibPath) + ".pack"))
                                File.Delete(Path.GetFileName(localLibPath) + ".pack");
                        }
                        catch (IOException)
                        {
                            //Ignore
                        }

                        using (XZStream xzStream =
                            new XZStream(File.OpenRead(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack.xz"))))
                        using (FileStream output = File.OpenWrite(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack")))
                        {
                            xzStream.CopyTo(output);
                        }

                        try
                        {
                            File.Delete(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack.xz"));
                        }
                        catch (IOException)
                        {
                            //Ignore
                        }

                        Log("Extracted to " + Path.GetFileName(localLibPath) + ".pack");
                        Log("Unpacking...");
                        byte[] decompressed = File.ReadAllBytes(Path.Combine(tempFolder,
                            Path.GetFileName(localLibPath) + ".pack"));

                        string end =
                            Encoding.UTF8.GetString(
                                decompressed.Skip(decompressed.Length - 4).Take(4).ToArray());
                        if (end != "SIGN")
                        {
                            Log("Unpacking failed, signature missing " + end);
                            Log("Failed to unpack " + name + ". Looks like a download issue. Try relaunching.");
                            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                                MessageBox.Show("Failed to unpack packed library " + name + ". Aborting Launch!");
                        }
                        else
                        {
                            int x = decompressed.Length;
                            int len = decompressed[x - 8] & 255 | (decompressed[x - 7] & 255) << 8 |
                                      (decompressed[x - 6] & 255) << 16 |
                                      (decompressed[x - 5] & 255) << 24;
                            Log("  Signed");
                            Log("  Checksum Length: " + len);
                            Log("  Total Length:    " + (decompressed.Length - len - 8));

                            Log("Stripping checksum from file...");

                            File.Delete(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack"));

                            using (FileStream s = File.OpenWrite(Path.Combine(tempFolder,
                                Path.GetFileName(localLibPath) + ".pack")))
                            {
                                s.Write(decompressed, 0, decompressed.Length - len - 8);
                            }

                            if (currentOs == "windows")
                            {
                                string javaPath = Path.Combine(Environment.GetEnvironmentVariable("PROGRAMFILES"), "Java");

                                Log("Successfully removed integrated pack file checksum.");
                                Log("Finding Unpack200 instance...");
                                Log("Searching Java installation at " + javaPath);

                                bool success = false;

                                try
                                {
                                    foreach (var installation in Directory.EnumerateDirectories(javaPath))
                                    {
                                        Log("Searching " + installation);

                                        //If there's not a bin folder in this subfolder, move on
                                        if (!Directory.EnumerateDirectories(installation).Any(subfolder => subfolder.Contains("bin")))
                                            continue;

                                        //Get the path to unpack200
                                        string unpack200 = Path.Combine(installation, "bin", "unpack200.exe");
                                        if (File.Exists(unpack200))
                                        {
                                            Log("Found valid UP200 Instance at " + unpack200 + ", using...");
                                            Log(unpack200 +
                                                $" \"{Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack")}\" \"{localLibPath}\"");
                                            Process proc = Process.Start(new ProcessStartInfo
                                            {
                                                FileName = unpack200,
                                                Arguments =
                                                    $"\"{Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack")}\" \"{localLibPath}\"",
                                                CreateNoWindow = true
                                            });

                                            proc.WaitForExit();
                                            Log("UP200 Exited.");
                                            if (File.Exists(localLibPath))
                                            {
                                                Log("Successfully unpacked");
                                                File.Delete(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack"));
                                                success = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            Log(
                                                "Found a supposedly valid java installation that's missing unpack200! It should be at " +
                                                unpack200);
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    success = false;
                                }

                                if (!success)
                                {
                                    Log("Failed to unpack " + name + ". Is Java installed?");
                                    MessageBox.Show(
                                        $"Failed to unpack packed library {name}. Aborting Launch!");
                                    return;
                                }
                            }
                            else
                            {
                                //If java is installed on Linux or Mac, unpack200 SHOULD be in PATH as it installs to /usr/bin
                                Log(
                                    "Non-Windows OS Detected. Assuming java is installed (which it should be, if you're going to try to run minecraft), we should be able to call unpack200 directly.");

                                Process proc = Process.Start(new ProcessStartInfo
                                {
                                    FileName = "unpack200",
                                    Arguments =
                                        $"\"{Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack")}\" \"{localLibPath}\"",
                                    CreateNoWindow = true
                                });

                                Log("Invoke: " + proc.StartInfo.FileName + " " + proc.StartInfo.Arguments);

                                proc.WaitForExit();
                                Log("UP200 Exited.");
                                if (File.Exists(localLibPath))
                                {
                                    Log("Successfully unpacked");
                                    File.Delete(Path.Combine(tempFolder, Path.GetFileName(localLibPath) + ".pack"));
                                    break;
                                }
                            }
                        }
                    }
                }

                using (FileStream stream = File.Exists(forgeVerMan) ? File.OpenWrite(forgeVerMan) : File.Create(forgeVerMan))
                using (StreamWriter fvmWriter = new StreamWriter(stream))
                {
                    fvmWriter.BaseStream.Seek(0, SeekOrigin.End);
                    fvmWriter.WriteLine(p.fmlVers);
                    Log("Saved installed state of forge " + p.fmlVers);
                }

                Log("Finished Getting forge libs. Getting mods...");
            }

            SetProgressSafe(81);
            SetStatusSafe("Getting Mods");

            string packFolder = Path.Combine(launcherDirectory, "packs", p.packName);

            if (!Directory.Exists(packFolder)) Directory.CreateDirectory(packFolder);

            string instVersFile = Path.Combine(packFolder, "installed");
            Version installedOverrideVers = default(Version);
            bool fileExists = false;
            List<string> installedMods = new List<string>();

            if (File.Exists(instVersFile))
            {
                fileExists = true;
                //Format: First line is override version, rest are mod file ids.
                using (StreamReader reader = File.OpenText(instVersFile))
                {
                    string verString = reader.ReadLine();
                    installedOverrideVers.FromString(verString);

                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        installedMods.Add(line);
                    }
                }
            }
            else
            {
                File.CreateText(instVersFile).Close();
            }


            bool updated = false;
            List<string> justInstalledMods = new List<string>();
            if (p.mods.Count > 0)
            {
                string modFolder = Path.Combine(packFolder, "mods");
                if (!Directory.Exists(modFolder)) Directory.CreateDirectory(modFolder);

                percentPer = 14f / p.mods.Count;

                foreach (Mod m in p.mods)
                {
                    //If this exact file is already in the installed file, skip it.
                    if (installedMods.Any(im => im.Split('|')[1] == m.FileId)) continue;

                    //If another version of the same mod is installed, remove it.
                    if (installedMods.FirstOrDefault(im => im.Split('|')[0] == m.Slug) is string oldVer)
                    {
                        if (Directory.EnumerateFiles(modFolder).FirstOrDefault(fileName =>
                                Path.GetFileNameWithoutExtension(fileName).Split('-')[0] ==
                                m.FileName.Split('-')[0] ||
                                Path.GetFileNameWithoutExtension(fileName).Split('_')[0] ==
                                m.FileName.Split('_')[0]) is
                            string oldVersion)
                        {
                            Log("Removing old version " + oldVersion + " to make room for " + m.FileName);
                            File.Delete(oldVersion);
                            installedMods.Remove(oldVer);
                        }
                    }

                    string downloadUrl = m.Url + "/download";

                    Log("GET " + downloadUrl);
                    SetProgressSafe(GetProgressRaw() + percentPer);
                    SetStatusSafe("Downloading " + m.Name + "...");

                    string modFileName = m.FileName;

                    foreach (char forbidden in Path.GetInvalidFileNameChars())
                    {
                        if (modFileName.Contains(forbidden))
                        {
                            modFileName = modFileName.Replace(forbidden, '_');
                        }
                    }

                    DownloadFile(downloadUrl,
                        Path.Combine(modFolder,
                            modFileName)); //Some bizzare mods (e.g betterfps) use backslashes in the filename, which obviously won't work

                    justInstalledMods.Add(m.Slug + "|" + m.FileId);
                }

                List<string> newInstalledMods = new List<string>();

                foreach (string installedMod in installedMods)
                {
                    string[] split = installedMod.Split('|');
                    if (!p.mods.Any(modInPack => modInPack.Slug == split[0] && modInPack.FileId == split[1]))
                    {
                        //Mod is no longer installed
                        Log("Mod " + split[0] + " is no longer in the pack but is installed. Removing it...");
                        Mod wasInstalled = new Mod
                        {
                            Slug = split[0],
                            FileId = split[1]
                        };

                        wasInstalled.Resolve();
                        string fName = wasInstalled.FileName;
                        if (Directory.EnumerateFiles(modFolder).FirstOrDefault(fileName => fileName == fName) is string oldPath)
                        {
                            File.Delete(oldPath);
                            Log("Deleted: " + oldPath);
                        }
                    }
                    else
                    {
                        newInstalledMods.Add(installedMod);
                    }
                }

                installedMods = newInstalledMods;
            }

            if (installedOverrideVers != p.version)
            {
                Log("Getting overrides");
                SetProgressSafe(96);
                SetStatusSafe("Installing overrides...");

                HttpWebRequest
                    req = WebRequest.CreateHttp(
                        "https://launcher.samboycoding.me/api/packoverrides/" + p.packId);

                bool overrides = false;

                using (HttpWebResponse verifyResponse = GetResponseNoException(req))
                {
                    if (verifyResponse.StatusCode == HttpStatusCode.NoContent)
                    {
                        Log("No overrides");
                    }
                    else
                    {
                        overrides = true;
                    }
                }

                if (overrides)
                {
                    SetStatusSafe("Downloading overrides...");
                    Log("Downloading overrides");
                    DownloadFile("https://launcher.samboycoding.me/api/packoverrides/" + p.packId,
                        Path.Combine(packFolder, "overrides.zip"));
                    SetStatusSafe("Extracting overrides...");
                    SetProgressSafe(98);
                    Log("Extracting overrides");
                    using (ZipFile file = ZipFile.Read(Path.Combine(packFolder, "overrides.zip")))
                    {
                        file.ExtractAll(packFolder, ExtractExistingFileAction.OverwriteSilently);
                    }
                }

                installedOverrideVers = p.version;
                updated = true;

                Log("Done.");
            }

            if (updated || justInstalledMods.Count > 0)
            {
                if (fileExists) File.Delete(instVersFile);

                using (StreamWriter writer = File.CreateText(instVersFile))
                {
                    writer.WriteLine(installedOverrideVers);
                    foreach (var m in justInstalledMods)
                    {
                        writer.WriteLine(m);
                    }

                    foreach (var m in installedMods)
                    {
                        writer.WriteLine(m);
                    }
                }
            }


            SetProgressSafe(100);
            SetStatusSafe("Launching...");

            if (!string.IsNullOrWhiteSpace(p.fmlVers))
                if (forgeVersionManifest == null)
                    forgeVersionManifest = JObject.Parse(File.ReadAllText(Path.Combine(forgeVersionFolder, "version.json")));

            if (versionManifest == null)
                versionManifest = JObject.Parse(File.ReadAllText(Path.Combine(vanillaVersionFolder, "version.json")));


            string args;
            string main;
            string versionName;
            if (!string.IsNullOrWhiteSpace(p.fmlVers))
            {
                args = forgeVersionManifest["minecraftArguments"].ToString();
                main = forgeVersionManifest["mainClass"].ToString();
                versionName = forgeVersionManifest["id"].ToString();
            }
            else
            {
                args = "";
                if (versionManifest.ContainsKey("arguments"))
                {
                    JArray gameArgs = (JArray) versionManifest["arguments"]["game"];
                    foreach (JToken arg in gameArgs)
                    {
                        if (arg is JObject) continue;

                        args += arg.Value<string>() + " ";
                    }

                    args = args.Trim();
                }
                else
                {
                    args = versionManifest["minecraftArguments"].ToString();
                }

                main = versionManifest["mainClass"].ToString();

                versionName = p.mcVers;
            }

            //Handle pre-releases
            string mcVersRaw = p.mcVers.Contains("-") ? p.mcVers.Substring(0, p.mcVers.IndexOf("-", StringComparison.Ordinal)) : p.mcVers;
            Version mcVers = new Version(mcVersRaw);

            args = args.Replace("${auth_player_name}", _main.playerName)
                .Replace("${version_name}", versionName)
                .Replace("${game_directory}", "\"" + packFolder + "\"");

            if (mcVers < new Version("1.7.2"))
            {
                //Old assets
                args = args.Replace("${game_assets}",
                    "\"" + Path.Combine(launcherDirectory, "assets", "virtual", "legacy") + "\"");
            }
            else
            {
                args = args.Replace("${assets_root}",
                    "\"" + Path.Combine(launcherDirectory, "assets") + "\"");
                //New assets
            }

            args = args.Replace("${assets_index_name}", p.mcVers)
                .Replace("${auth_uuid}", _main.playerUUID)
                .Replace("${auth_access_token}", _main.accessToken)
                .Replace("${auth_session}", _main.accessToken)
                .Replace("${user_properties}", "{}")
                .Replace("${user_type}", "mojang")
                .Replace("${version_type}", "forge");

            float freeRamMB;
            if (currentOs == "windows")
            {
                var performance = new PerformanceCounter("Memory", "Available MBytes");
                freeRamMB = performance.NextValue();
            }
            else if (currentOs == "linux")
            {
                Process memInfo = Process.Start(new ProcessStartInfo
                {
                    FileName = "free",
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                });
                memInfo.WaitForExit();

                string output = memInfo.StandardOutput.ReadToEnd();
                string[] lines = output.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

                //Format of "free" is header row, followed by mem row, followed by Swap row
                //So we want the second row, index 1
                string line = lines[1];

                string[] columns = line.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                //Columns are Type, Total, Used, Free, Shared, buff/cache, available
                //Free is the 4th column, index 3

                string freeMemRaw = columns[3];
                freeRamMB = float.Parse(freeMemRaw) / 1024; //Given in KiB, so convert to MiB
            }
            else
            {
                //OSX
                freeRamMB = 4096;
            }

            Log(freeRamMB + " MB of RAM available.");
            ulong memGB = (ulong) Math.Floor(freeRamMB / 1024f);
            Log("That's " + memGB + "GB of RAM");

            ulong memAllocated = memGB <= 7 ? memGB : 6;
            Log("Therefore we're allocating " + memAllocated + "GB of RAM");

            string cp = "";

            string sep = currentOs == "windows" ? ";" : ":";

            versionManifest =
                JObject.Parse(File.ReadAllText(Path.Combine(vanillaVersionFolder, "version.json")));

            libraries = new List<JObject>(versionManifest["libraries"]
                .Where(lib => !((JObject) lib).ContainsKey("natives"))
                .Select(lib => (JObject) lib));

            foreach (var lib in libraries)
            {
                url = lib["downloads"]["artifact"]["url"].ToString();
                string path = Path.Combine(libsFolder, Environment.OSVersion.Platform == PlatformID.Win32NT
                    ? url.Replace("https://libraries.minecraft.net/", "")
                        .Replace("/", "\\")
                    : url.Replace("https://libraries.minecraft.net/", ""));
                cp += path + sep;
            }

            if (!string.IsNullOrWhiteSpace(p.fmlVers))
            {
                foreach (var o in (JArray) forgeVersionManifest["libraries"])
                {
                    JObject lib = (JObject) o;
                    string name = lib["name"].ToString();

                    if (name.Contains("net.minecraftforge:forge")) continue;

                    string[] split = name.Split(':');

                    string path = split[0].Split('.').Aggregate("", (current, subfolder) => current + subfolder + "/");

                    path += split[1] + "/" + split[2] + "/" + split[1] + "-" + split[2] + ".jar";
                    string localLibPath = Path.Combine(libsFolder,
                        Environment.OSVersion.Platform == PlatformID.Win32NT ? path.Replace("/", "\\") : path);

                    if (cp.Split(new[] {sep}, StringSplitOptions.None)
                            .FirstOrDefault(en => en.Contains(split[1])) is string entry &&
                        entry != null) //Dupe lib check
                    {
                        string version = Path.GetFileNameWithoutExtension(entry).Replace(split[1] + "-", "");
                        int pos = string.Compare(version, split[2], StringComparison.Ordinal);
                        if (pos < 0)
                        {
                            Log("Replacing library " + split[1] + " version " + version + " with later version " +
                                split[2]);
                            cp = cp.Replace(entry, localLibPath);
                        }
                        else if (pos == 0)
                        {
                            if (entry != localLibPath)
                            {
                                Log("WARNING! Unable to tell the different between " + localLibPath + " and " +
                                    entry);
                            }

                            continue; //To be safe, don't add.
                        }
                        else
                        {
                            Log("Not adding " + name + " to classpath - later version " + entry +
                                " already in cp.");
                            continue;
                        }
                    }

                    cp += localLibPath + sep;
                }

                cp += Path.Combine(forgeVersionFolder, "forge.jar") + sep;
            }

            cp += Path.Combine(vanillaVersionFolder, "minecraft.jar");

            Process mc = Process.Start(new ProcessStartInfo
            {
                FileName = "java" + (currentOs == "windows" ? "w" : ""),
                Arguments =
                    $"-Xmx{memAllocated}G -Xms{memAllocated - 1}G -Djava.net.preferIPv4Stack=true " +
                    $"-Djava.library.path=\"{nativesFolder}\" -Dminecraft.applet.TargetDirectory=\"{packFolder}\" " +
                    $"-classpath \"{cp}\" -XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 " +
                    "-XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 " +
                    $"-XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M {main} {args}",
                WorkingDirectory = packFolder,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            });

            Log(mc.StartInfo.FileName + " " + mc.StartInfo.Arguments);

            Log("\r\n\r\n\r\n\r\n\r\n");

            _main.SwitchToFullscreenLog();

            while (!mc.HasExited || !mc.StandardOutput.EndOfStream)
            {
                char[] block = new char[100];
                mc.StandardOutput.ReadBlock(block, 0, 100);
                _main.LogRaw(new string(block).Replace("\n", "\r\n"));
            }

            Log(mc.StandardError.ReadToEnd());

            _main.RevertFullscreenLog();

            Log("Process exited with result " + mc.ExitCode);

            Log("\r\n\r\n\r\n\r\n\r\n");
        }
    }
}