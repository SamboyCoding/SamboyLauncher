﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace SamboyLauncherNew
{
    public partial class MainForm : Form
    {
        private const string CHANGELOG_URL =
            "https://gitlab.com/SamboyCoding/SamboyLauncher/raw/master/CHANGELOG";

        private readonly PrivateFontCollection pfc = new PrivateFontCollection();
        internal static MainForm Instance;

        internal string playerUUID;
        internal string playerName;
        internal string accessToken;

        private bool justUpdated;
        private bool _autoLoggingBackIn;

        private List<Pack> packs = new List<Pack>();
        internal string launcherDirectory = "";

        public MainForm(string[] args)
        {
            InitializeComponent();
            Instance = this;
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            Application.ThreadException += OnThreadException;

            Icon = Properties.Resources.samboylauncher512;

            log.VisibleChanged += (sender, e) =>
            {
                if (log.Visible)
                {
                    log.SelectionStart = log.TextLength;
                    log.ScrollToCaret();
                }
            };

            if (args.Contains("--updated"))
            {
                justUpdated = true;
            }
        }

        public bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors == SslPolicyErrors.None) return true;

            foreach (var chainStatus in chain.ChainStatus)
            {
                if (chainStatus.Status == X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    continue;
                }

                chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                bool chainIsValid = chain.Build((X509Certificate2) certificate);
                if (chainIsValid) continue;

                Log(certificate.ToString(true) + " is invalid!");

                isOk = false;
                break;
            }

            return isOk;
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = CertificateValidationCallback;

            DateTime t = DateTime.Now;
            Log("Initializing Application...");

            Log("Configuring Launcher Directory...");
            launcherDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "SamboyLauncher_New");
            Directory.CreateDirectory(launcherDirectory);

            Log("Launcher Directory: " + launcherDirectory);

            Log("Setting up visual styles...");

            if (!File.Exists(Path.Combine(launcherDirectory, "Roboto.ttf")))
            {
                Log("Grabbing Resource: Roboto.ttf");
                DownloadFile("https://launcher.samboycoding.me/resources/roboto", Path.Combine(launcherDirectory, "Roboto.ttf"));
            }

            if (!File.Exists(Path.Combine(launcherDirectory, "RobotoMono.ttf")))
            {
                Log("Grabbing Resource: RobotoMono.ttf");
                DownloadFile("https://launcher.samboycoding.me/resources/mono", Path.Combine(launcherDirectory, "RobotoMono.ttf"));
            }

            pfc.AddFontFile(Path.Combine(launcherDirectory, "Roboto.ttf"));
            pfc.AddFontFile(Path.Combine(launcherDirectory, "RobotoMono.ttf"));

            Font = new Font(pfc.Families[0], 9);

            packselect.Font = new Font(pfc.Families[0], 12);
            log.Font = new Font(pfc.Families[1], 9);
            packname.Font = new Font(pfc.Families[0], 16);
            launchbutton.Hide();
            launchbutton.Font = new Font(pfc.Families[0], 9);
            installstatus.Font = launchbutton.Font;
            packdescription.Font = launchbutton.Font;
            modlist.Font = packselect.Font;
            mcvers.Font = launchbutton.Font;
            forgeversion.Font = launchbutton.Font;
            usernamelabel.Font = launchbutton.Font;
            passwordlabel.Font = launchbutton.Font;
            username.Font = launchbutton.Font;
            password.Font = launchbutton.Font;
            rememberme.Font = launchbutton.Font;
            loginbutton.Font = launchbutton.Font;
            logincontrols.Font = launchbutton.Font;
            changelog.Font = launchbutton.Font;
            changelogText.Font = new Font(pfc.Families[1], 12);

            modlist.WordWrap = false;

            packname.Text = "No Pack Selected";
            packdescription.Text = "Select a pack on the left.";

            packversion.Hide();
            progress.Hide();
            mcvers.Hide();
            forgeversion.Hide();
            installstatus.Hide();
            modlist.Hide();
            launchbutton.Hide();
            changelog.Hide();

            log.SelectionStart = log.TextLength;

            log.WordWrap = false;

            Log($"Application initialized in {(DateTime.Now - t).TotalMilliseconds}ms");

            string authFile = Path.Combine(launcherDirectory, "authdata");

            if (!File.Exists(authFile))
            {
                ShowLoginControls();
            }
        }

        private void OnFormShown(object sender, EventArgs e)
        {
            Log("Showing Window...\r\n");

            Version currentLauncherVersion = new Version();
            currentLauncherVersion.FromString(typeof(MainForm).Assembly.GetName().Version.ToString(3));

            if (justUpdated)
            {
                Log("Apparantly we've just been updated. Verifying...");
                string lastInstalledLauncherVersionString =
                    File.ReadAllText(Path.Combine(launcherDirectory, "launcher.current"));

                Version lastInstalledLauncherVersion = new Version();
                lastInstalledLauncherVersion.FromString(lastInstalledLauncherVersionString);

                if (currentLauncherVersion > lastInstalledLauncherVersion)
                {
                    try
                    {
                        Log("Launcher has just been updated from version " + lastInstalledLauncherVersion + " to " +
                            currentLauncherVersion);
                        Log("Fetching changelog...");

                        Log("0 bytes => " + CHANGELOG_URL);
                        string fullChangelog = (string) DownloadFile(CHANGELOG_URL);
                        Log(fullChangelog.Length + " bytes <= " + CHANGELOG_URL);

                        string[] versions = fullChangelog.Split(new[] {"=========="}, StringSplitOptions.None);
                        Dictionary<Version, string> changeLogs = new Dictionary<Version, string>();

                        foreach (string changelogItem in versions)
                        {
                            string[] split = changelogItem.Split(new[] {"----------"}, StringSplitOptions.None);
                            Version version = new Version();
                            version.FromString(split[0]);

                            if (version <= lastInstalledLauncherVersion || version > currentLauncherVersion)
                            {
                                continue; //Skip irrelevant changelogs.
                            }

                            changeLogs.Add(version, split[1]);
                        }

                        changelog.Show();
                        logsplitter.Hide();

                        string text = "The launcher's just been updated from " + lastInstalledLauncherVersion + " to " +
                                      currentLauncherVersion + "!";

                        text += "\r\nHere's what's changed:\r\n";

                        foreach (KeyValuePair<Version, string> pair in changeLogs)
                        {
                            text += "\r\nIn Version " + pair.Key + ": \r\n";
                            text += pair.Value.Replace("\n", "\r\n") + "\r\n";
                        }

                        changelogText.Text = text;
                    }
                    catch (Exception ex)
                    {
                        Log("Exception showing changelog! " + ex);
                    }
                }
                else
                {
                    Log("No, that's not correct. Was installed: " + lastInstalledLauncherVersion + " now: " +
                        currentLauncherVersion + "\r\n");
                }
            }

            Log("Beginning update check.");
            Log("Current version is " + currentLauncherVersion);
            File.WriteAllText(Path.Combine(launcherDirectory, "launcher.current"), currentLauncherVersion.ToString());

            try
            {
                Version latestLauncherVersion = new Version();
                latestLauncherVersion.FromString(
                    (string) DownloadFile("https://launcher.samboycoding.me/api/launcher/updates/"));

                Log("Latest available version is " + latestLauncherVersion);

                if (latestLauncherVersion > currentLauncherVersion)
                {
                    Log("Updating Launcher...");
                    string updaterPath = Path.Combine(launcherDirectory, "Updater.exe");
                    DownloadFile("https://launcher.samboycoding.me/api/launcher/updater", updaterPath);
                    Process.Start(new ProcessStartInfo
                    {
                        FileName = updaterPath,
                        Arguments = "\"" + typeof(MainForm).Assembly.CodeBase.Replace("file:///", "") + "\""
                    });
                }
                else
                {
                    Log("No update required.\r\n");
                }
            }
            catch (Exception ex)
            {
                Log("Exception checking for/installing launcher updates! " + ex);
            }

            //Run the startup functions in a different thread
            Task.Run(() =>
            {
                try
                {
                    Log("Grabbing packs list...");
                    //Get the pack list
                    Log("0 bytes => GET /api/packs");
                    HttpWebRequest request = WebRequest.CreateHttp("https://launcher.samboycoding.me/api/packs");

                    using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                    using (Stream rs = response.GetResponseStream())
                    using (StreamReader responseStream = new StreamReader(rs))
                    {
                        Log($"{response.ContentLength} bytes <= GET /api/packs");
                        JArray rawPacks = JArray.Parse(responseStream.ReadToEnd());
                        Log($"Deserializing {rawPacks.Count} packs...");

                        foreach (var o in rawPacks)
                        {
                            JObject packRaw = (JObject) o;
                            packs.Add(new Pack
                            {
                                packId = packRaw["ID"].ToObject<uint>(),
                                packName = packRaw["PACK_NAME"].ToString(),
                                url = new Uri("https://launcher.samboycoding.me/api/packs/" + packRaw["ID"])
                            });
                        }

                        Log($"Loaded {packs.Count} packs.\r\n");

                        foreach (Pack p in packs)
                        {
                            RunOnUIThread(() => { packselect.Items.Add(p.packName); });
                        }

                        string authFile = Path.Combine(launcherDirectory, "authdata");

                        if (File.Exists(authFile))
                        {
                            Log("Verifying Session With Mojang...");
                            RunOnUIThread(() => launchbutton.Enabled = false);

                            try
                            {
                                string[] authData = new List<string>(File.ReadAllLines(authFile)
                                        .Where(line => !string.IsNullOrWhiteSpace(line)))
                                    .ToArray();
                                string clientId = authData[0];
                                accessToken = authData[1];
                                playerUUID = authData[2];
                                playerName = authData[3];
                                string savedUsername = null;
                                string savedPassword = null;

                                if (authData.Length > 4)
                                {
                                    savedUsername = authData[4];
                                    savedPassword = authData[5];
                                }


                                HttpWebRequest verifyRequest =
                                    WebRequest.CreateHttp("https://authserver.mojang.com/validate");

                                verifyRequest.Method = "POST";

                                verifyRequest.ContentType = "application/json";

                                string data = @"{
                                    ""accessToken"": """ + accessToken + @""",
                                    ""clientToken"": """ + clientId + @"""
                                }";

                                verifyRequest.ContentLength = data.Length;
                                Log(verifyRequest.ContentLength + " bytes => " + verifyRequest.RequestUri);

                                using (StreamWriter writer = new StreamWriter(verifyRequest.GetRequestStream()))
                                {
                                    writer.Write(data);
                                }

                                using (HttpWebResponse verifyResponse = GetResponseNoException(verifyRequest))
                                {
                                    Log((verifyResponse.ContentLength < 0 ? 0 : verifyResponse.ContentLength)
                                        + " bytes <= " + verifyRequest.RequestUri);
                                    if (verifyResponse.StatusCode == HttpStatusCode.NoContent)
                                    {
                                        //We good.
                                        Log("Session Valid\r\n");
                                        RunOnUIThread(() => launchbutton.Enabled = true);
                                    }
                                    else if (verifyResponse.StatusCode == HttpStatusCode.Forbidden)
                                    {
                                        //Session invalid
                                        Log("Session Invalid\r\n");

                                        //Clear saved data
                                        accessToken = "";
                                        playerUUID = "";
                                        playerName = "";

                                        if (savedUsername != null)
                                        {
                                            username.Text = savedUsername;
                                            password.Text = savedPassword;
                                            _autoLoggingBackIn = true;
                                            rememberme.Checked = true;

                                            Log("Password Saved. Logging you back in...");

                                            DoLogin(loginbutton, EventArgs.Empty);

                                            _autoLoggingBackIn = false;
                                            Log("Logged Back in.\r\n");
                                        }
                                        else
                                        {
                                            ShowLoginControls();
                                            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                                                MessageBox.Show(
                                                    "Your session is invalid - did you sign into another launcher? Please sign in again.");
                                        }
                                    }
                                }
                            }
                            catch (IndexOutOfRangeException ex)
                            {
                                Log(ex);
                                ShowLoginControls();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    OnThreadException(this, new ThreadExceptionEventArgs(ex));
                }
            });
        }

        private void OnRememberMeClicked(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox) sender;

            if (checkBox.Checked && !_autoLoggingBackIn)
            {
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    DialogResult r = MessageBox.Show(
                        "WARNING: Checking this box causes us to save your credentials to the disk, in plain text. " +
                        "It does allow us to log you back in, if and when your session expires, but YOU SHOULD " +
                        "NOT DO THIS if anyone else can access this PC easily, such as if it is in a public place." +
                        "\r\nAre you sure you want to do this?",
                        "Confirmation", MessageBoxButtons.YesNo);

                    if (r != DialogResult.Yes)
                    {
                        checkBox.Checked = false;
                    }
                }
            }
        }

        private void DoLogin(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(username.Text))
            {
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                    MessageBox.Show("Please enter a username", "Error");
                return;
            }

            if (string.IsNullOrWhiteSpace(password.Text))
            {
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                    MessageBox.Show("Please enter a password", "Error");
                return;
            }

            Log("Logging In...");

            string clientId = null;

            string authFile = Path.Combine(launcherDirectory, "authdata");

            try
            {
                if (File.Exists(authFile))
                {
                    string[] authData =
                        new List<string>(File.ReadAllLines(authFile).Where(line => !string.IsNullOrWhiteSpace(line)))
                            .ToArray();
                    clientId = authData[0];
                    Log("We have already been assigned a client ID of " + clientId + ", using it...");
                }
            }
            catch (IndexOutOfRangeException)
            {
            }

            HttpWebRequest loginRequest = WebRequest.CreateHttp("https://authserver.mojang.com/authenticate");

            loginRequest.Method = "POST";

            loginRequest.ContentType = "application/json";

            string data = @"{
                ""agent"": {
                    ""name"": ""Minecraft"",
                    ""version"": 1

                },
                ""username"": """ + username.Text + @""",
                ""password"": """ + password.Text + @""","
                          + (clientId == null ? "" : @"""clientToken"": """ + clientId + "\",") +
                          @"""requestUser"": true
            }";

            loginRequest.ContentLength = data.Length;

            Log(loginRequest.ContentLength + " bytes => " + loginRequest.RequestUri);

            using (StreamWriter writer = new StreamWriter(loginRequest.GetRequestStream()))
            {
                writer.Write(data);
            }

            using (HttpWebResponse loginResponse = GetResponseNoException(loginRequest))
            using (Stream s = loginResponse.GetResponseStream())
            using (StreamReader sr = new StreamReader(s))
            {
                Log(loginResponse.ContentLength + " bytes <= " + loginRequest.RequestUri);
                JObject response = JObject.Parse(sr.ReadToEnd());
                if (loginResponse.StatusCode == HttpStatusCode.OK)
                {
                    Log("Saving client/access token pair...");
                    if (File.Exists(authFile))
                    {
                        File.Delete(authFile);
                    }

                    using (StreamWriter writer = File.CreateText(authFile))
                    {
                        writer.Write(
                            $"{response["clientToken"]}\r\n{response["accessToken"]}\r\n{response["selectedProfile"]["id"]}\r\n{response["selectedProfile"]["name"]}");
                        if (rememberme.Checked)
                        {
                            Log("Saving username/password as per user request...");
                            writer.Write($"\r\n{username.Text}\r\n{password.Text}");
                        }
                    }

                    playerUUID = response["selectedProfile"]["id"].ToString();
                    playerName = response["selectedProfile"]["name"].ToString();
                    accessToken = response["accessToken"].ToString();

                    Log("Login Complete.\r\n");
                    RunOnUIThread(() => launchbutton.Enabled = true);
                    HideLoginControls();
                }
                else if (loginResponse.StatusCode == HttpStatusCode.Forbidden)
                {
                    if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                        MessageBox.Show(response["errorMessage"].ToString());
                    Log(response["errorMessage"] + "\r\n");
                    ShowLoginControls();
                }
            }
        }

        private void ShowLoginControls()
        {
            RunOnUIThread(() =>
            {
                logsplitter.Hide();
                logincontrols.Show();
            });
        }

        private void HideLoginControls()
        {
            RunOnUIThread(() =>
            {
                logsplitter.Show();
                logincontrols.Hide();
            });
        }

        private void OnThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Console.WriteLine(e.Exception);
            Log("\r\n\r\n");
            Log(
                "---------------------------------------------------------------------------------------------------------UNHANDLED THREAD EXCEPTION---------------------------------------------------------------------------------------------------------");
            Log("\r\n");
            Log(e.Exception);
            Log("\r\n");
            Log(
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Log("\r\n\r\n");
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Hide();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                MessageBox.Show("One of the background threads has hit a problem. We can't recover from this.\r\n\r\n" +
                                "This means that the application is about to exit, and will do as soon as you click OK.\r\n" +
                                "There's nothing I can do.\r\n" +
                                "The exception details are as follows. Please report this: \r\n\r\n" + e.ExceptionObject,
                    "Application Error", MessageBoxButtons.OK);

            Environment.Exit(-1);
        }

        internal void Log(object t)
        {
            RunOnUIThread(() =>
            {
                int initialPos = log.SelectionStart;
                bool wasAtBottom = initialPos == log.TextLength;
                //log.Text += t + "\r\n";
                log.SuspendPainting();
                log.AppendText(t + "\r\n");
                log.ResumePainting();

                log.SelectionStart = wasAtBottom ? log.TextLength : initialPos;

                log.ScrollToCaret();
            });
        }

        internal void LogRaw(object t)
        {
            RunOnUIThread(() =>
            {
                int initialPos = log.SelectionStart;
                bool wasAtBottom = initialPos == log.TextLength;
                log.SuspendPainting();
                log.AppendText(t.ToString());
                log.ResumePainting();
                //log.Text += t.ToString();

                log.SelectionStart = wasAtBottom ? log.TextLength : initialPos;

                log.ScrollToCaret();
            });
        }

        private void ChangeColorOfPackSelectBackground(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;

            ListBox listBox = (ListBox) sender;
            e.DrawBackground();
            Brush myBrush = Brushes.WhiteSmoke;

            e.Graphics.FillRectangle(
                (e.State & DrawItemState.Selected) == DrawItemState.Selected
                    ? new SolidBrush(Color.FromArgb(38, 50, 56))
                    : Brushes.Transparent, e.Bounds);

            e.Graphics.DrawString(listBox.Items[e.Index].ToString(), e.Font, myBrush, e.Bounds);
            e.DrawFocusRectangle();
        }

        private void Packselect_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            ListBox listBox = (ListBox) sender;
            e.ItemHeight = listBox.Font.Height;
        }

        private void OnPackSelect(object sender, EventArgs e)
        {
            ListBox listBox = (ListBox) sender;

            int index = listBox.SelectedIndex;

            Pack p = packs[index];

            Task.Run(() =>
            {
                try
                {
                    Log("Getting Pack Info...");
                    Log($"0 bytes => GET {p.url}");
                    HttpWebRequest request = WebRequest.CreateHttp(p.url);

                    using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                    using (Stream rs = response.GetResponseStream())
                    using (StreamReader responseStream = new StreamReader(rs))
                    {
                        Log($"{response.ContentLength} bytes <= GET {p.url}");
                        JArray responseData = JArray.Parse(responseStream.ReadToEnd());
                        Log("Deserializing pack...");

                        JObject rawPack = (JObject) responseData[0];

                        string vers = rawPack["VERSION"].ToString();

                        p.version.FromString(vers);

                        p.description = rawPack["DESCRIPTION"].ToString();

                        p.fmlVers = rawPack["FORGE_VERSION"].ToString();
                        p.mcVers = rawPack["GAME_VERSION"].ToString();
                        p.mods = new List<Mod>();

                        if (!string.IsNullOrWhiteSpace(rawPack["MODS"].ToString()))
                        {
                            foreach (string modUrl in rawPack["MODS"].ToString().Split(','))
                            {
                                var match = Regex.Match(modUrl,
                                    @"http[s]?:\/\/minecraft\.curseforge\.com\/projects\/(?<slug>[a-z0-9\-]+)\/files\/(?<fileid>[0-9]+)");

                                Mod m = new Mod
                                {
                                    Url = new Uri(modUrl),
                                    Slug = match.Groups["slug"].Value,
                                    FileId = match.Groups["fileid"].Value
                                };

                                p.mods.Add(m);
                            }
                        }

                        packs[index] = p;

                        Log("Got Pack Info\r\n");

                        RunOnUIThread(() =>
                        {
                            packname.Text = p.packName;
                            packdescription.Text = p.description.Replace("\\n", "\r\n");
                            packversion.Text = $"Version {p.version}";
                            mcvers.Text = "Minecraft Version " + p.mcVers;
                            forgeversion.Text = "Minecraft Forge " + p.fmlVers;

                            UpdateModList(p);

                            packversion.Show();
                            modlist.Show();
                            mcvers.Show();
                            forgeversion.Show();
                            launchbutton.Show();

                            launchbutton.BringToFront();
                            mcvers.SendToBack();
                            forgeversion.SendToBack();
                        });

                        Log("Resolving Mods...");

                        List<Mod> mods = new List<Mod>(p.mods);

                        var i = -1;
                        new Thread(() =>
                        {
                            foreach (Mod mod in mods)
                            {
                                i++;
                                Log(mod);
                                mod.Resolve();
                                lock (p.mods)
                                {
                                    p.mods[i] = mod;
                                }

                                Log("Resolved mod: " + mod);
                                packs[index] = p;
                                RunOnUIThread(() => UpdateModList(p));
                            }
                        })
                        {
                            IsBackground = true
                        }.Start();
                    }
                }
                catch
                    (Exception ex)
                {
                    OnThreadException(this, new ThreadExceptionEventArgs(ex));
                }
            });
        }

        private void UpdateModList(Pack p)
        {
            string text = $"Pack contains {p.mods.Count} mods:\r\n\r\n";

            lock (p.mods)
            {
                foreach (Mod m in p.mods)
                {
                    text += "• " + m + "\r\n";
                }
            }

            modlist.Text = text;
        }

        private Pack launchingPack;

        private void DoLaunch(object sender, EventArgs e)
        {
            Log("About to launch pack. Spinning up launcher thread...");

            launchingPack = packs[packselect.SelectedIndex];

            launchbutton.Enabled = false;
            SetProgressSafe(0);
            progress.Show();
            installstatus.Text = "Spinning Up Launch Thread...";
            installstatus.Show();

            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                Task.Run(() => { HandleActualLaunch(null); });
            }
            else
            {
                Thread workingThread = new Thread(HandleActualLaunch) {IsBackground = true};

                workingThread.Start();
            }
        }

        private void HandleActualLaunch(object o)
        {
            try
            {
                GameLauncher.LaunchPack(launcherDirectory, launchingPack, this);
                launchbutton.Enabled = true;
                progress.Hide();
                installstatus.Hide();
            }
            catch (Exception exception)
            {
                OnThreadException(this, new ThreadExceptionEventArgs(exception));
            }
        }

        internal void SetStatusSafe(string status)
        {
            RunOnUIThread(() => installstatus.Text = status);
        }

        private float rawProgress;
        private int prevPos;

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal float GetProgressRaw()
        {
            return rawProgress;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal void SetProgressSafe(float prg)
        {
            rawProgress = prg;
            RunOnUIThread(() => progress.Value = (int) Math.Round(rawProgress));
        }

        private void RunOnUIThread(Action a)
        {
            Invoke((MethodInvoker) delegate { a(); });
        }

        internal static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            try
            {
                return (HttpWebResponse) req.GetResponse();
            }
            catch (WebException we)
            {
                if (!(we.Response is HttpWebResponse resp))
                    throw;
                return resp;
            }
        }

        /// <summary>
        /// Downloads a file, and either returns the contents, or, if dest is provided, saves it to a file specified by dest.
        /// </summary>
        /// <param name="source">The URL to get the file from</param>
        /// <param name="dest">The destination to save the file to. If null (default) then the contents of the file are returned.</param>
        /// <param name="suppress">Whether to not print any exceptions that occur</param>
        /// <returns>The contents of the file (or null if the download failed) if dest is null or not provided, or true if the file was saved, or
        /// false if it wasn't.</returns>
        internal object DownloadFile(string source, string dest = null, bool suppress = false)
        {
            HttpWebRequest request = WebRequest.CreateHttp(source);
//We're chrome 63 now
            request.UserAgent =
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
            try
            {
                if (dest != null)
                {
                    WebClient Client = new WebClient();
                    Client.DownloadFile(source, dest);
                    return true;
                }

                using (HttpWebResponse response = GetResponseNoException(request))
                using (Stream responseStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    if (response.StatusCode == HttpStatusCode.OK) return reader.ReadToEnd();

//Bad response code
                    Log("Response code of " + response.StatusCode + " received from " + source + "!");
                    Log(reader.ReadToEnd());
                    return null;
                }
            }
            catch (Exception e)
            {
                if (!suppress) Log(e);
                return dest == null ? (object) null : false;
            }
        }

        private void DismissChangelog(object sender, EventArgs e)
        {
            changelog.Hide();
            if (playerUUID != "")
                logsplitter.Show();
            else
                ShowLoginControls();
        }

        private void OnLogScroll(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
//Scroll up
                if (log.SelectionStart > 200)
                {
                    log.SelectionStart -= 200;
                }
                else
                {
                    log.SelectionStart = 0;
                }

                log.ScrollToCaret();
            }
            else
            {
                if (log.SelectionStart < log.TextLength - 200)
                {
                    log.SelectionStart += 200;
                }
                else
                {
                    log.SelectionStart = log.TextLength;
                }

                log.ScrollToCaret();
            }
        }

        public void SwitchToFullscreenLog()
        {
            RunOnUIThread(() =>
            {
                prevPos = logsplitter.SplitterDistance;
                logsplitter.Panel1MinSize = 0;
                logsplitter.SplitterDistance = 0;
                launchbutton.Enabled = true;
                installstatus.Hide();
                progress.Hide();
            });
        }

        public void RevertFullscreenLog()
        {
            RunOnUIThread(() => { logsplitter.SplitterDistance = prevPos; });
        }
    }
}