﻿using System;
using System.Collections.Generic;
using System.IO;
using HtmlAgilityPack;

namespace SamboyLauncherNew
{
    public struct Mod
    {
        static readonly HtmlWeb web = new HtmlWeb();
        private static Dictionary<string, Tuple<string, string>> cachedModEntries;
        private static string cacheFilePath;

        public string Slug;
        public string Name;
        public string FileName;

        public Uri Url;
        public string FileId;

        private Exception resolveError;

        public bool IsResolved()
        {
            return Name != null && resolveError == null;
        }

        public bool HasErrored()
        {
            return resolveError != null;
        }

        public override string ToString()
        {
            if (IsResolved())
            {
                return $"{Name} ({FileName})";
            }

            if (resolveError != null)
            {
                return $"Error resolving {Slug + "_" + FileId} => {resolveError.GetType().FullName}:{resolveError.Message}";
            }

            return $"Resolving details for \"{Slug}\" version {FileId}...";
        }

        public void Resolve()
        {
            if (Url == null)
                Url = new Uri($"https://minecraft.curseforge.com/projects/{Slug}/files/{FileId}");

            if (cachedModEntries == null)
            {
                cachedModEntries = new Dictionary<string, Tuple<string, string>>();
                lock (cachedModEntries)
                {
                    cacheFilePath = Path.Combine(MainForm.Instance.launcherDirectory, "cachedmods");
                    if (!File.Exists(cacheFilePath)) File.Create(cacheFilePath).Dispose();
                    MainForm.Instance.Log($"Loading Cached Mod Data from {cacheFilePath}...");

                    string cached = File.ReadAllText(cacheFilePath);

                    if (!string.IsNullOrWhiteSpace(cached))
                    {
                        foreach (string line in cached.Split(new[] {"\r\n"}, StringSplitOptions.None))
                        {
                            if (string.IsNullOrWhiteSpace(line)) continue;
                            string[] split = line.Split('|');
                            cachedModEntries.Add(split[0], new Tuple<string, string>(split[1], split[2]));
                        }

                        MainForm.Instance.Log($"Loaded {cachedModEntries.Count} cached mods.");
                    }
                }
            }

            lock (cachedModEntries)
            {
                if (cachedModEntries.ContainsKey(Slug + "_" + FileId))
                {
                    Tuple<string, string> data = cachedModEntries[Slug + "_" + FileId];
                    Name = data.Item1;
                    FileName = data.Item2;
                    return;
                }
            }


            try
            {
                var doc = web.Load(Url);
                Name = doc.DocumentNode.SelectSingleNode("//meta[@property='og:title']").Attributes["content"].DeEntitizeValue;
                FileName = doc.DocumentNode.SelectSingleNode("//meta[@property='og:description']").Attributes["content"].DeEntitizeValue;
                if (!FileName.EndsWith(".jar")) FileName = FileName + ".jar";
            }
            catch (Exception e)
            {
                resolveError = e;
                MainForm.Instance.Log(e);
                return;
            }

            cachedModEntries.Add(Slug + "_" + FileId, new Tuple<string, string>(Name, FileName));

            string lines = "";

            foreach (var cachedMod in cachedModEntries)
            {
                lines += $"{cachedMod.Key}|{cachedMod.Value.Item1}|{cachedMod.Value.Item2}\r\n";
            }

            File.WriteAllText(cacheFilePath, lines);
        }
    }
}