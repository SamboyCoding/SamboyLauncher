﻿using System;

namespace SamboyLauncherNew
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logsplitter = new System.Windows.Forms.SplitContainer();
            this.packdetailssplitter = new System.Windows.Forms.SplitContainer();
            this.packselect = new System.Windows.Forms.ListBox();
            this.forgeversion = new System.Windows.Forms.Label();
            this.modlist = new System.Windows.Forms.TextBox();
            this.mcvers = new System.Windows.Forms.Label();
            this.installstatus = new System.Windows.Forms.Label();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.launchbutton = new System.Windows.Forms.Button();
            this.packdescription = new System.Windows.Forms.TextBox();
            this.packversion = new System.Windows.Forms.Label();
            this.packname = new System.Windows.Forms.Label();
            this.log = new BetterTextBox();
            this.logincontrols = new System.Windows.Forms.GroupBox();
            this.passwordlabel = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.usernamelabel = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.loginbutton = new System.Windows.Forms.Button();
            this.rememberme = new System.Windows.Forms.CheckBox();
            this.changelog = new System.Windows.Forms.GroupBox();
            this.changelogText = new System.Windows.Forms.TextBox();
            this.changelogDoneButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logsplitter)).BeginInit();
            this.logsplitter.Panel1.SuspendLayout();
            this.logsplitter.Panel2.SuspendLayout();
            this.logsplitter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packdetailssplitter)).BeginInit();
            this.packdetailssplitter.Panel1.SuspendLayout();
            this.packdetailssplitter.Panel2.SuspendLayout();
            this.packdetailssplitter.SuspendLayout();
            this.logincontrols.SuspendLayout();
            this.SuspendLayout();
            //
            // logsplitter
            //
            this.logsplitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.logsplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logsplitter.Location = new System.Drawing.Point(0, 0);
            this.logsplitter.Name = "logsplitter";
            this.logsplitter.Orientation = System.Windows.Forms.Orientation.Horizontal;
            //
            // logsplitter.Panel1
            //
            this.logsplitter.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.logsplitter.Panel1.Controls.Add(this.packdetailssplitter);
            //
            // logsplitter.Panel2
            //
            this.logsplitter.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.logsplitter.Panel2.Controls.Add(this.log);
            this.logsplitter.Size = new System.Drawing.Size(1134, 605);
            this.logsplitter.SplitterDistance = 403;
            this.logsplitter.SplitterWidth = 2;
            this.logsplitter.TabIndex = 0;
            //
            // packdetailssplitter
            //
            this.packdetailssplitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.packdetailssplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.packdetailssplitter.Location = new System.Drawing.Point(0, 0);
            this.packdetailssplitter.Name = "packdetailssplitter";
            //
            // packdetailssplitter.Panel1
            //
            this.packdetailssplitter.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packdetailssplitter.Panel1.Controls.Add(this.packselect);
            //
            // packdetailssplitter.Panel2
            //
            this.packdetailssplitter.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packdetailssplitter.Panel2.Controls.Add(this.forgeversion);
            this.packdetailssplitter.Panel2.Controls.Add(this.modlist);
            this.packdetailssplitter.Panel2.Controls.Add(this.mcvers);
            this.packdetailssplitter.Panel2.Controls.Add(this.installstatus);
            this.packdetailssplitter.Panel2.Controls.Add(this.progress);
            this.packdetailssplitter.Panel2.Controls.Add(this.launchbutton);
            this.packdetailssplitter.Panel2.Controls.Add(this.packdescription);
            this.packdetailssplitter.Panel2.Controls.Add(this.packversion);
            this.packdetailssplitter.Panel2.Controls.Add(this.packname);
            this.packdetailssplitter.Size = new System.Drawing.Size(1134, 403);
            this.packdetailssplitter.SplitterDistance = 240;
            this.packdetailssplitter.SplitterWidth = 1;
            this.packdetailssplitter.TabIndex = 3;
            //
            // packselect
            //
            this.packselect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.packselect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packselect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packselect.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.packselect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packselect.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packselect.FormattingEnabled = true;
            this.packselect.IntegralHeight = false;
            this.packselect.ItemHeight = 20;
            this.packselect.Location = new System.Drawing.Point(12, 12);
            this.packselect.Name = "packselect";
            this.packselect.Size = new System.Drawing.Size(225, 390);
            this.packselect.TabIndex = 1;
            this.packselect.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.ChangeColorOfPackSelectBackground);
            this.packselect.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.Packselect_MeasureItem);
            this.packselect.SelectedIndexChanged += new System.EventHandler(this.OnPackSelect);
            //
            // forgeversion
            //
            this.forgeversion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.forgeversion.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forgeversion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.forgeversion.Location = new System.Drawing.Point(387, 26);
            this.forgeversion.Name = "forgeversion";
            this.forgeversion.Size = new System.Drawing.Size(500, 14);
            this.forgeversion.TabIndex = 11;
            this.forgeversion.Text = "Minecraft Forge 10.13.4.1614";
            this.forgeversion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            // modlist
            //
            this.modlist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modlist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.modlist.Font = new System.Drawing.Font("Roboto", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modlist.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modlist.Location = new System.Drawing.Point(508, 63);
            this.modlist.Multiline = true;
            this.modlist.Name = "modlist";
            this.modlist.ReadOnly = true;
            this.modlist.Size = new System.Drawing.Size(379, 282);
            this.modlist.TabIndex = 10;
            this.modlist.Text = "Mod List";
            //
            // mcvers
            //
            this.mcvers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mcvers.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcvers.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.mcvers.Location = new System.Drawing.Point(387, 12);
            this.mcvers.Name = "mcvers";
            this.mcvers.Size = new System.Drawing.Size(500, 14);
            this.mcvers.TabIndex = 9;
            this.mcvers.Text = "Minecraft Version 1.7.10";
            this.mcvers.TextAlign = System.Drawing.ContentAlignment.TopRight;
            //
            // installstatus
            //
            this.installstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.installstatus.AutoSize = true;
            this.installstatus.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.installstatus.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.installstatus.Location = new System.Drawing.Point(19, 348);
            this.installstatus.Name = "installstatus";
            this.installstatus.Size = new System.Drawing.Size(161, 14);
            this.installstatus.TabIndex = 8;
            this.installstatus.Text = "Gyrating HydroGyroscopes...";
            //
            // progress
            //
            this.progress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.progress.Location = new System.Drawing.Point(19, 367);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(784, 23);
            this.progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progress.TabIndex = 7;
            this.progress.Value = 30;
            //
            // launchbutton
            //
            this.launchbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.launchbutton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.launchbutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.launchbutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.launchbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.launchbutton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.launchbutton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.launchbutton.Location = new System.Drawing.Point(809, 367);
            this.launchbutton.Name = "launchbutton";
            this.launchbutton.Size = new System.Drawing.Size(75, 23);
            this.launchbutton.TabIndex = 6;
            this.launchbutton.Text = "Launch";
            this.launchbutton.UseVisualStyleBackColor = true;
            this.launchbutton.Click += new System.EventHandler(this.DoLaunch);
            //
            // packdescription
            //
            this.packdescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.packdescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packdescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packdescription.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packdescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packdescription.Location = new System.Drawing.Point(16, 63);
            this.packdescription.Multiline = true;
            this.packdescription.Name = "packdescription";
            this.packdescription.ReadOnly = true;
            this.packdescription.Size = new System.Drawing.Size(357, 282);
            this.packdescription.TabIndex = 5;
            this.packdescription.Text = "Pack Description";
            //
            // packversion
            //
            this.packversion.AutoSize = true;
            this.packversion.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packversion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packversion.Location = new System.Drawing.Point(16, 37);
            this.packversion.Name = "packversion";
            this.packversion.Size = new System.Drawing.Size(78, 14);
            this.packversion.TabIndex = 4;
            this.packversion.Text = "Pack Version";
            //
            // packname
            //
            this.packname.AutoSize = true;
            this.packname.Font = new System.Drawing.Font("Roboto", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packname.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packname.Location = new System.Drawing.Point(14, 12);
            this.packname.Name = "packname";
            this.packname.Size = new System.Drawing.Size(118, 25);
            this.packname.TabIndex = 3;
            this.packname.Text = "Pack Name";
            //
            // log
            //
            this.log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.log.Location = new System.Drawing.Point(3, 4);
            this.log.Multiline = true;
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(1128, 205);
            this.log.MouseWheel += this.OnLogScroll;
            this.log.TabIndex = 0;
            //
            // logincontrols
            //
            this.logincontrols.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logincontrols.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.logincontrols.Controls.Add(this.rememberme);
            this.logincontrols.Controls.Add(this.passwordlabel);
            this.logincontrols.Controls.Add(this.password);
            this.logincontrols.Controls.Add(this.usernamelabel);
            this.logincontrols.Controls.Add(this.username);
            this.logincontrols.Controls.Add(this.loginbutton);
            this.logincontrols.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.logincontrols.Location = new System.Drawing.Point(424, 143);
            this.logincontrols.Name = "logincontrols";
            this.logincontrols.Size = new System.Drawing.Size(299, 348);
            this.logincontrols.TabIndex = 12;
            this.logincontrols.TabStop = false;
            this.logincontrols.Text = "Login";
            this.logincontrols.Visible = false;
            //
            // passwordlabel
            //
            this.passwordlabel.AutoSize = true;
            this.passwordlabel.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordlabel.Location = new System.Drawing.Point(6, 77);
            this.passwordlabel.Name = "passwordlabel";
            this.passwordlabel.Size = new System.Drawing.Size(61, 14);
            this.passwordlabel.TabIndex = 3;
            this.passwordlabel.Text = "Password";
            //
            // password
            //
            this.password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.password.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.password.Location = new System.Drawing.Point(6, 94);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(287, 22);
            this.password.TabIndex = 2;
            this.password.UseSystemPasswordChar = true;
            //
            // usernamelabel
            //
            this.usernamelabel.AutoSize = true;
            this.usernamelabel.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernamelabel.Location = new System.Drawing.Point(6, 29);
            this.usernamelabel.Name = "usernamelabel";
            this.usernamelabel.Size = new System.Drawing.Size(145, 14);
            this.usernamelabel.TabIndex = 1;
            this.usernamelabel.Text = "Username/Email Address";
            //
            // username
            //
            this.username.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.username.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.username.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.username.Location = new System.Drawing.Point(6, 46);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(287, 22);
            this.username.TabIndex = 0;
            //
            // loginbutton
            //
            this.loginbutton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loginbutton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.loginbutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.loginbutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.loginbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginbutton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginbutton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.loginbutton.Location = new System.Drawing.Point(9, 293);
            this.loginbutton.Name = "loginbutton";
            this.loginbutton.Size = new System.Drawing.Size(284, 49);
            this.loginbutton.Click += new EventHandler(this.DoLogin);
            this.loginbutton.TabIndex = 12;
            this.loginbutton.Text = "Login";
            this.loginbutton.UseVisualStyleBackColor = true;
            //
            // rememberme
            //
            this.rememberme.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rememberme.AutoSize = true;
            this.rememberme.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rememberme.Location = new System.Drawing.Point(9, 123);
            this.rememberme.MaximumSize = new System.Drawing.Size(290, 0);
            this.rememberme.Name = "rememberme";
            this.rememberme.Size = new System.Drawing.Size(215, 30);
            this.rememberme.TabIndex = 13;
            this.rememberme.CheckedChanged += new EventHandler(this.OnRememberMeClicked);
            this.rememberme.Text = "Save My Username/Password To Disk\r\n(DO NOT CHECK if you share your PC)";
            this.rememberme.UseVisualStyleBackColor = true;
            //
            // changelog
            //
            this.changelog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                                                               | System.Windows.Forms.AnchorStyles.Left)
                                                                              | System.Windows.Forms.AnchorStyles.Right)));
            this.changelog.Controls.Add(this.changelogText);
            this.changelog.Controls.Add(this.changelogDoneButton);
            this.changelog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.changelog.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.changelog.Location = new System.Drawing.Point(300, 110);
            this.changelog.Name = "changelog";
            this.changelog.Size = new System.Drawing.Size(550, 400);
            this.changelog.TabIndex = 12;
            this.changelog.TabStop = false;
            this.changelog.Text = "What's New";
            this.changelog.Visible = false;
            //
            // changelogText
            //
            this.changelogText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.changelogText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.changelogText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.changelogText.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.changelogText.Location = new System.Drawing.Point(4, 14);
            this.changelogText.Multiline = true;
            this.changelogText.Name = "changelogText";
            this.changelogText.ReadOnly = true;
            this.changelogText.Size = new System.Drawing.Size(542, 350);
            this.changelogText.Text = "[Loading Changelog]";
            this.changelogText.TabIndex = 0;
            //
            // changelogDoneButton
            //
            this.changelogDoneButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom)));
            this.changelogDoneButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.changelogDoneButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.changelogDoneButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.changelogDoneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changelogDoneButton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changelogDoneButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.changelogDoneButton.Location = new System.Drawing.Point(4, 365);
            this.changelogDoneButton.Name = "changelogDoneButton";
            this.changelogDoneButton.Size = new System.Drawing.Size(540, 30);
            this.changelogDoneButton.TabIndex = 6;
            this.changelogDoneButton.Text = "Done";
            this.changelogDoneButton.UseVisualStyleBackColor = true;
            this.changelogDoneButton.Click += new System.EventHandler(this.DismissChangelog);
            //
            // MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(1134, 611);
            this.Controls.Add(this.logincontrols);
            this.Controls.Add(this.changelog);
            this.Controls.Add(this.logsplitter);
            this.MinimumSize = new System.Drawing.Size(1150, 650);
            this.Name = "MainForm";
            this.Text = "Samboy Launcher";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Shown += new System.EventHandler(this.OnFormShown);
            this.logsplitter.Panel1.ResumeLayout(false);
            this.logsplitter.Panel2.ResumeLayout(false);
            this.logsplitter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logsplitter)).EndInit();
            this.logsplitter.ResumeLayout(false);
            this.packdetailssplitter.Panel1.ResumeLayout(false);
            this.packdetailssplitter.Panel2.ResumeLayout(false);
            this.packdetailssplitter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.packdetailssplitter)).EndInit();
            this.packdetailssplitter.ResumeLayout(false);
            this.logincontrols.ResumeLayout(false);
            this.logincontrols.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer logsplitter;
        private BetterTextBox log;
        private System.Windows.Forms.SplitContainer packdetailssplitter;
        private System.Windows.Forms.Label packversion;
        private System.Windows.Forms.Label packname;
        private System.Windows.Forms.ListBox packselect;
        private System.Windows.Forms.TextBox packdescription;
        private System.Windows.Forms.Button launchbutton;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Label installstatus;
        private System.Windows.Forms.Label forgeversion;
        private System.Windows.Forms.TextBox modlist;
        private System.Windows.Forms.Label mcvers;
        private System.Windows.Forms.GroupBox logincontrols;
        private System.Windows.Forms.Label passwordlabel;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label usernamelabel;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Button loginbutton;
        private System.Windows.Forms.CheckBox rememberme;
        private System.Windows.Forms.GroupBox changelog;
        private System.Windows.Forms.TextBox changelogText;
        private System.Windows.Forms.Button changelogDoneButton;
    }
}

