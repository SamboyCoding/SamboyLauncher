﻿using System.Collections.Generic;

namespace PackCreationTool
{
    public struct ModSearchResult
    {
        public string url;
        public string name;
        public string slug;
        public List<ModVersion> versions;
        public List<ModSearchResult> dependencies;

        public override string ToString()
        {
            return name;
        }
    }
}