﻿using System;

namespace PackCreationTool
{
    partial class CreationToolMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modSearchResults = new System.Windows.Forms.ListBox();
            this.modSearchBox = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splittter = new System.Windows.Forms.SplitContainer();
            this.removeModButton = new System.Windows.Forms.Button();
            this.addModButton = new System.Windows.Forms.Button();
            this.modVersionSelect = new System.Windows.Forms.ComboBox();
            this.releaseType = new System.Windows.Forms.ComboBox();
            this.modDescription = new System.Windows.Forms.TextBox();
            this.modName = new System.Windows.Forms.Label();
            this.packDescription = new System.Windows.Forms.TextBox();
            this.forgeVersion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mcVers = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.packVersion = new System.Windows.Forms.TextBox();
            this.savePackButton = new System.Windows.Forms.Button();
            this.openPackButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.packName = new System.Windows.Forms.TextBox();
            this.modsInPack = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.splittter)).BeginInit();
            this.splittter.Panel1.SuspendLayout();
            this.splittter.Panel2.SuspendLayout();
            this.splittter.SuspendLayout();
            this.SuspendLayout();
            //
            // modSearchResults
            //
            this.modSearchResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.modSearchResults.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modSearchResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modSearchResults.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modSearchResults.FormattingEnabled = true;
            this.modSearchResults.Items.AddRange(new object[] {
            "Search Results"});
            this.modSearchResults.Location = new System.Drawing.Point(9, 13);
            this.modSearchResults.Name = "modSearchResults";
            this.modSearchResults.Size = new System.Drawing.Size(119, 379);
            this.modSearchResults.TabIndex = 0;
            this.modSearchResults.SelectedIndexChanged += new System.EventHandler(this.OnSearchItemClicked);
            //
            // modSearchBox
            //
            this.modSearchBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modSearchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modSearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modSearchBox.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modSearchBox.Location = new System.Drawing.Point(134, 12);
            this.modSearchBox.Name = "modSearchBox";
            this.modSearchBox.Size = new System.Drawing.Size(535, 20);
            this.modSearchBox.TabIndex = 1;
            this.modSearchBox.Text = "Search for mods...";
            this.modSearchBox.Enabled = false;
            this.modSearchBox.TextChanged += new System.EventHandler(this.SearchParamUpdated);
            this.modSearchBox.Enter += new System.EventHandler(this.OnSearchBoxFocus);
            this.modSearchBox.Leave += new System.EventHandler(this.OnSearchBoxUnFocus);
            //
            // splitter1
            //
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 435);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            //
            // splittter
            //
            this.splittter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.splittter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splittter.Location = new System.Drawing.Point(3, 0);
            this.splittter.Name = "splittter";
            //
            // splittter.Panel1
            //
            this.splittter.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.splittter.Panel1.Controls.Add(this.removeModButton);
            this.splittter.Panel1.Controls.Add(this.addModButton);
            this.splittter.Panel1.Controls.Add(this.modVersionSelect);
            this.splittter.Panel1.Controls.Add(this.releaseType);
            this.splittter.Panel1.Controls.Add(this.modDescription);
            this.splittter.Panel1.Controls.Add(this.modName);
            this.splittter.Panel1.Controls.Add(this.modSearchBox);
            this.splittter.Panel1.Controls.Add(this.modSearchResults);
            //
            // splittter.Panel2
            //
            this.splittter.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.splittter.Panel2.Controls.Add(this.packDescription);
            this.splittter.Panel2.Controls.Add(this.forgeVersion);
            this.splittter.Panel2.Controls.Add(this.label4);
            this.splittter.Panel2.Controls.Add(this.mcVers);
            this.splittter.Panel2.Controls.Add(this.label3);
            this.splittter.Panel2.Controls.Add(this.label2);
            this.splittter.Panel2.Controls.Add(this.packVersion);
            this.splittter.Panel2.Controls.Add(this.savePackButton);
            this.splittter.Panel2.Controls.Add(this.openPackButton);
            this.splittter.Panel2.Controls.Add(this.label1);
            this.splittter.Panel2.Controls.Add(this.packName);
            this.splittter.Panel2.Controls.Add(this.modsInPack);
            this.splittter.Size = new System.Drawing.Size(1113, 435);
            this.splittter.SplitterDistance = 683;
            this.splittter.TabIndex = 3;
            //
            // removeModButton
            //
            this.removeModButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeModButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.removeModButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.removeModButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.removeModButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.removeModButton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeModButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.removeModButton.Location = new System.Drawing.Point(513, 400);
            this.removeModButton.Name = "removeModButton";
            this.removeModButton.Size = new System.Drawing.Size(75, 23);
            this.removeModButton.Enabled = false;
            this.removeModButton.Click += new EventHandler(this.DoRemoveMod);
            this.removeModButton.TabIndex = 13;
            this.removeModButton.Text = "Remove";
            this.removeModButton.UseVisualStyleBackColor = true;
            //
            // addModButton
            //
            this.addModButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addModButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.addModButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.addModButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.addModButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addModButton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addModButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.addModButton.Location = new System.Drawing.Point(594, 400);
            this.addModButton.Click += new EventHandler(this.DoAddMod);
            this.addModButton.Name = "addModButton";
            this.addModButton.Size = new System.Drawing.Size(75, 23);
            this.addModButton.TabIndex = 12;
            this.addModButton.Text = "Add";
            this.addModButton.UseVisualStyleBackColor = true;
            //
            // modVersionSelect
            //
            this.modVersionSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modVersionSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modVersionSelect.Enabled = false;
            this.modVersionSelect.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modVersionSelect.FormattingEnabled = true;
            this.modVersionSelect.Location = new System.Drawing.Point(307, 401);
            this.modVersionSelect.Name = "modVersionSelect";
            this.modVersionSelect.Size = new System.Drawing.Size(200, 21);
            this.modVersionSelect.TabIndex = 5;
            this.modVersionSelect.Text = "(Version)";
            //
            // releaseType
            //
            this.releaseType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left))));
            this.releaseType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.releaseType.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.releaseType.FormattingEnabled = true;
            this.releaseType.Items.AddRange(new object[] {
                ReleaseType.RELEASE,
                ReleaseType.BETA,
                ReleaseType.ALPHA
            });
            this.releaseType.Location = new System.Drawing.Point(96, 401);
            this.releaseType.Name = "releaseType";
            this.releaseType.Size = new System.Drawing.Size(205, 21);
            this.releaseType.TabIndex = 4;
            this.releaseType.Enabled = false;
            this.releaseType.SelectedIndexChanged += new EventHandler(this.OnReleaseTypeSelected);
            this.releaseType.Text = "(Release Type)";
            //
            // modDescription
            //
            this.modDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modDescription.Location = new System.Drawing.Point(139, 72);
            this.modDescription.Multiline = true;
            this.modDescription.Name = "modDescription";
            this.modDescription.ReadOnly = true;
            this.modDescription.Size = new System.Drawing.Size(530, 320);
            this.modDescription.TabIndex = 3;
            this.modDescription.Text = "Search for and select a mod...";
            this.modDescription.WordWrap = false;
            //
            // modName
            //
            this.modName.AutoSize = true;
            this.modName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modName.Location = new System.Drawing.Point(134, 39);
            this.modName.Name = "modName";
            this.modName.Size = new System.Drawing.Size(201, 29);
            this.modName.TabIndex = 2;
            this.modName.Text = "No Mod Selected";
            //
            // packDescription
            //
            this.packDescription.Anchor =
            ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom |
                                                    System.Windows.Forms.AnchorStyles.Top))));
            this.packDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.packDescription.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packDescription.Location = new System.Drawing.Point(10, 155);
            this.packDescription.Multiline = true;
            this.packDescription.Name = "packDescription";
            this.packDescription.Size = new System.Drawing.Size(288, 237);
            this.packDescription.TabIndex = 13;
            this.packDescription.Text = "My Cool Pack\'s Cool Description";
            this.packDescription.WordWrap = false;
            //
            // forgeVersion
            //
            this.forgeVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.forgeVersion.FormattingEnabled = true;
            this.forgeVersion.Location = new System.Drawing.Point(117, 128);
            this.forgeVersion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.forgeVersion.Name = "forgeVersion";
            this.forgeVersion.Size = new System.Drawing.Size(181, 21);
            this.forgeVersion.SelectedIndexChanged += new EventHandler(this.OnForgeVersionSelected);
            this.forgeVersion.Enabled = false;
            this.forgeVersion.TabIndex = 18;
            //
            // label4
            //
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(24, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Forge Version:";
            //
            // mcVers
            //
            this.mcVers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.mcVers.FormattingEnabled = true;
            this.mcVers.Location = new System.Drawing.Point(117, 101);
            this.mcVers.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.mcVers.SelectedIndexChanged += new EventHandler(this.OnMcVersSelected);
            this.mcVers.Name = "mcVers";
            this.mcVers.Size = new System.Drawing.Size(181, 21);
            this.mcVers.TabIndex = 16;
            //
            // label3
            //
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(7, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Minecraft Version:";
            //
            // label2
            //
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(26, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Pack Version:";
            //
            // packVersion
            //
            this.packVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.packVersion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packVersion.Location = new System.Drawing.Point(117, 74);
            this.packVersion.Name = "packVersion";
            this.packVersion.Size = new System.Drawing.Size(181, 20);
            this.packVersion.TabIndex = 12;
            this.packVersion.Text = "1.0";
            //
            // savePackButton
            //
            this.savePackButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.savePackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.savePackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.savePackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.savePackButton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savePackButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.savePackButton.Location = new System.Drawing.Point(223, 17);
            this.savePackButton.Name = "savePackButton";
            this.savePackButton.Click += new EventHandler(this.SavePack);
            this.savePackButton.Size = new System.Drawing.Size(75, 23);
            this.savePackButton.TabIndex = 11;
            this.savePackButton.Text = "Save";
            this.savePackButton.UseVisualStyleBackColor = true;
            //
            // openPackButton
            //
            this.openPackButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            this.openPackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.openPackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.openPackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openPackButton.Font = new System.Drawing.Font("Roboto", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openPackButton.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.openPackButton.Location = new System.Drawing.Point(10, 17);
            this.openPackButton.Name = "openPackButton";
            this.openPackButton.Size = new System.Drawing.Size(75, 23);
            this.openPackButton.Click += new EventHandler(this.LoadPack);
            this.openPackButton.TabIndex = 10;
            this.openPackButton.Text = "Browse";
            this.openPackButton.UseVisualStyleBackColor = true;
            //
            // label1
            //
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(33, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Pack Name:";
            //
            // packName
            //
            this.packName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.packName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.packName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.packName.Location = new System.Drawing.Point(117, 46);
            this.packName.Name = "packName";
            this.packName.Size = new System.Drawing.Size(181, 20);
            this.packName.TabIndex = 8;
            this.packName.Text = "My Cool Pack";
            //
            // modsInPack
            //
            this.modsInPack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Top)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.modsInPack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.modsInPack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modsInPack.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.modsInPack.FormattingEnabled = true;
            this.modsInPack.Items.AddRange(new object[] {
            "Mods in the Pack"});
            this.modsInPack.Location = new System.Drawing.Point(304, 12);
            this.modsInPack.SelectedIndexChanged += new EventHandler(this.OnModInPackClicked);
            this.modsInPack.Name = "modsInPack";
            this.modsInPack.Size = new System.Drawing.Size(119, 379);
            this.modsInPack.TabIndex = 7;
            //
            // CreationToolMain
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(1116, 435);
            this.Controls.Add(this.splittter);
            this.Controls.Add(this.splitter1);
            this.MinimumSize = new System.Drawing.Size(1132, 474);
            this.Name = "CreationToolMain";
            this.Text = "Pack Creation Tool";
            this.Load += new EventHandler(this.OnFormLoad);
            this.splittter.Panel1.ResumeLayout(false);
            this.splittter.Panel1.PerformLayout();
            this.splittter.Panel2.ResumeLayout(false);
            this.splittter.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splittter)).EndInit();
            this.splittter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox modSearchResults;
        private System.Windows.Forms.TextBox modSearchBox;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.SplitContainer splittter;
        private System.Windows.Forms.ComboBox modVersionSelect;
        private System.Windows.Forms.ComboBox releaseType;
        private System.Windows.Forms.TextBox modDescription;
        private System.Windows.Forms.Label modName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox packName;
        private System.Windows.Forms.ListBox modsInPack;
        private System.Windows.Forms.Button addModButton;
        private System.Windows.Forms.Button savePackButton;
        private System.Windows.Forms.Button openPackButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox packVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox packDescription;
        private System.Windows.Forms.ComboBox forgeVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox mcVers;
        private System.Windows.Forms.Button removeModButton;
    }
}

