﻿using System.Collections.Generic;

namespace PackCreationTool
{
    public struct ModInPack
    {
        public string url;
        public string name;
        public string slug;
        public ModVersion selectedVersion;
        public List<ModVersion> otherVersions;

        public override string ToString()
        {
            if(!string.IsNullOrEmpty(name))
                return name + " (" + selectedVersion + ")";

            return slug + " (unknown version)";
        }

        public override bool Equals(object obj)
        {
            return this == obj;
        }

        public static bool operator ==(ModInPack thisMod, object other)
        {
            return other is ModInPack otherMod && thisMod.slug == otherMod.slug
                                               && (otherMod.selectedVersion == null || thisMod.selectedVersion == otherMod.selectedVersion);
        }

        public static bool operator !=(ModInPack thisO, object other)
        {
            return !(thisO == other);
        }
    }
}