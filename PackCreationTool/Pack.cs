﻿using System;
using System.Collections.Generic;

namespace PackCreationTool
{
    public struct Pack
    {
        public string packName;
        public string description;

        public Version version;

        public string mcVers;
        public string fmlVers;

        public List<ModSearchResult> mods;

        public Uri url;

        public override string ToString()
        {
            return $"Pack{{Name={packName}, version={version}, SourceURL={url}, mcVersion={mcVers}, " +
                   $"forgeVersion={fmlVers}, mods={mods?.Count ?? 0}}}";
        }

        public string GetModsHumanReadable()
        {
            string result = "";
            if (mods != null)
            {
                foreach (ModSearchResult m in mods)
                {
                    result += m.ToString();
                    result += "\r\n";
                }
            }

            return result;
        }
    }
}