﻿namespace PackCreationTool
{
    public struct ModVersion
    {
        public ReleaseType type;
        public string fileId;
        public string fileName;

        public override string ToString()
        {
            return fileName;
        }

        public static bool operator ==(ModVersion thisVersion, object other)
        {
            return other is ModVersion otherVer && thisVersion.fileId == otherVer.fileId;
        }

        public static bool operator !=(ModVersion thisVersion, object other)
        {
            return !(thisVersion == other);
        }
    }

    public enum ReleaseType
    {
        RELEASE, BETA, ALPHA
    }
}