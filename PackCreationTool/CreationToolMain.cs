﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace PackCreationTool
{
    public partial class CreationToolMain : Form
    {
        private readonly string modSearchPlaceHolder;
        private static readonly HtmlWeb web = new HtmlWeb();
        private readonly List<string> forgeVersions = new List<string>();

        private bool shouldUpdateVersion = true;
        private bool shouldHandleSearchClick = true;
        private bool shouldHandleModsInPackClick = true;

        public CreationToolMain()
        {
            InitializeComponent();
            modSearchPlaceHolder = modSearchBox.Text;
        }

        private void OnSearchBoxFocus(object sender, EventArgs e)
        {
            if (modSearchBox.Text == modSearchPlaceHolder)
            {
                modSearchBox.Text = "";
            }
        }

        private void OnSearchBoxUnFocus(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(modSearchBox.Text))
            {
                modSearchBox.Text = modSearchPlaceHolder;
            }
        }

        private void OnSearchItemClicked(object sender, EventArgs e)
        {
            ListBox box = (ListBox) sender;

            if (!(box.SelectedItem is ModSearchResult)) return;

            if (!shouldHandleSearchClick) return;

            shouldUpdateVersion = false;
            shouldHandleSearchClick = false;

            modsInPack.ClearSelected();

            ModSearchResult selectedModSearchResult = (ModSearchResult) box.SelectedItem;

            Task.Run(() =>
            {
                RunOnUIThread(() => modVersionSelect.Enabled = false);
                var doc = web.Load(selectedModSearchResult.url);

                if (Regex.IsMatch(selectedModSearchResult.slug, @"\d+"))
                {
                    string oldSlug = selectedModSearchResult.slug;
                    string realSlug = doc.DocumentNode.SelectSingleNode("//h1[contains(@class, \"project-title\")]/a")
                        .Attributes["href"].DeEntitizeValue.Replace("/projects/", "");

                    selectedModSearchResult.slug = realSlug;
                    selectedModSearchResult.url = selectedModSearchResult.url.Replace(oldSlug, realSlug);
                }

                string description = WebUtility.HtmlDecode(doc.DocumentNode
                    .SelectSingleNode("//div[@class=\"project-description\"]").InnerText);

                RunOnUIThread(() =>
                {
                    modDescription.Text = description;
                    modName.Text = selectedModSearchResult.name;
                    releaseType.Text = "(Release Type)";
                    modVersionSelect.Text = "(Version)";
                    addModButton.Text = "Add";
                    removeModButton.Enabled = false;
                });

                if (selectedModSearchResult.versions != null && selectedModSearchResult.versions.Count > 0) return;

                string selectedMCVers = null;

                RunOnUIThread(() => selectedMCVers = (string) mcVers.SelectedItem);

                selectedModSearchResult.versions = new List<ModVersion>();

                HttpWebRequest request =
                    WebRequest.CreateHttp($"https://api.cfwidget.com/minecraft/mc-mods/{selectedModSearchResult.slug}");

                using (HttpWebResponse response = GetResponseNoException(request))
                using (Stream rS = response.GetResponseStream())
                using (StreamReader reader =
                    new StreamReader(rS ?? throw new Exception("Failed to get response stream")))
                {
                    JObject modData = JObject.Parse(reader.ReadToEnd());
                    if (modData.ContainsKey("error") && modData["error"].ToString() == "in_queue")
                    {
                        RunOnUIThread(() =>
                        {
                            MessageBox.Show(
                                "Version info is being loaded - please try again in 30 seconds.",
                                "In Queue...");
                        });
                        shouldUpdateVersion = true;
                        shouldHandleSearchClick = true;
                        return;
                    }
                    else if (!modData.ContainsKey("error"))
                    {

                        JArray versions = (JArray) modData["files"];

                        foreach (var ver in versions)
                        {
                            if (((JArray) ver["versions"]).Any(mcVer => mcVer.ToString() == selectedMCVers)
                            ) //If this mod is designed to work with the mc version we have
                            {
                                selectedModSearchResult.versions.Add(new ModVersion
                                {
                                    fileId = ver["url"].ToString().Replace(
                                        $"https://www.curseforge.com/minecraft/mc-mods/{selectedModSearchResult.slug}/download/",
                                        ""),
                                    fileName = ver["name"].ToString().EndsWith(".jar")
                                        ? ver["name"].ToString()
                                        : ver["name"] + ".jar",
                                    type = (ReleaseType) Enum.Parse(typeof(ReleaseType), ver["type"].ToString().ToUpper())
                                });
                            }
                        }

                        selectedModSearchResult.name = modData["title"].ToString();
                    }

                    if (selectedModSearchResult.versions.Count == 0)
                    {
                        selectedModSearchResult.versions.Add(new ModVersion
                        {
                            fileId = "",
                            fileName = "[MOD DOES NOT WORK WITH THIS MC VERSION]",
                            type = ReleaseType.RELEASE
                        });

                        selectedModSearchResult.versions.Add(new ModVersion
                        {
                            fileId = "",
                            fileName = "[MOD DOES NOT WORK WITH THIS MC VERSION]",
                            type = ReleaseType.BETA
                        });

                        selectedModSearchResult.versions.Add(new ModVersion
                        {
                            fileId = "",
                            fileName = "[MOD DOES NOT WORK WITH THIS MC VERSION]",
                            type = ReleaseType.ALPHA
                        });

                        RunOnUIThread(() =>
                        {
                            box.Items.RemoveAt(modSearchResults.SelectedIndex);
                            modName.Text = "[MOD UNAVAILABLE FOR " + selectedMCVers + "]";
                            modDescription.Text = "[MOD UNAVAILABLE FOR " + selectedMCVers + "]";
                        });

                        shouldUpdateVersion = true;
                        shouldHandleSearchClick = true;
                        return;
                    }

                    RunOnUIThread(() => box.Items[box.SelectedIndex] = selectedModSearchResult);
                    RunOnUIThread(() => releaseType.Enabled = true);
                    shouldUpdateVersion = true;
                    shouldHandleSearchClick = true;
                }
            });
        }

        private void OnModInPackClicked(object sender, EventArgs e)
        {
            ListBox box = (ListBox) sender;

            if (!(box.SelectedItem is ModInPack)) return;
            if (!shouldHandleModsInPackClick) return;

            shouldUpdateVersion = false;
            shouldHandleModsInPackClick = false;

            modSearchResults.ClearSelected();
            modVersionSelect.Items.Clear();

            ModInPack selectedModInPack = (ModInPack) box.SelectedItem;
            selectedModInPack.otherVersions = new List<ModVersion>();

            Task.Run(() =>
            {
                RunOnUIThread(() => modVersionSelect.Enabled = false);
                var doc = web.Load(selectedModInPack.url);
                string description = WebUtility.HtmlDecode(doc.DocumentNode
                    .SelectSingleNode("//div[@class=\"project-description\"]").InnerText);

                RunOnUIThread(() =>
                {
                    modDescription.Text = description;
                    modName.Text = selectedModInPack.name;
                    releaseType.Text = "(Release Type)";
                    shouldUpdateVersion = false;
                    releaseType.SelectedIndex = -1;
                    modVersionSelect.Text = "(Version)";
                });

                string selectedMCVers = null;

                RunOnUIThread(() => selectedMCVers = (string) mcVers.SelectedItem);

                HttpWebRequest request =
                    WebRequest.CreateHttp($"https://api.cfwidget.com/minecraft/mc-mods/{selectedModInPack.slug}");

                using (HttpWebResponse response = GetResponseNoException(request))
                using (Stream rS = response.GetResponseStream())
                using (StreamReader reader =
                    new StreamReader(rS ?? throw new Exception("Failed to get response stream")))
                {
                    JObject modData = JObject.Parse(reader.ReadToEnd());
                    if (modData.ContainsKey("error") && modData["error"].ToString() == "in_queue")
                    {
                        RunOnUIThread(() =>
                        {
                            MessageBox.Show(
                                "Waiting for version listing. Please allow at least 30 seconds, at which point the release selection box will unlock.",
                                "In Queue...");
                        });

                        Task.Delay(30000).Wait();
                        OnSearchItemClicked(sender, e);
                        return;
                    } else if (!modData.ContainsKey("error"))
                    {
                        JArray rawVersions = (JArray) modData["files"];

                        foreach (var ver in rawVersions)
                        {
                            if (((JArray) ver["versions"]).Any(mcVer => mcVer.ToString() == selectedMCVers)
                            ) //If this mod is designed to work with the mc version we have
                            {
                                ModVersion v = new ModVersion
                                {
                                    fileId = ver["url"].ToString().Replace(
                                        $"https://www.curseforge.com/minecraft/mc-mods/{selectedModInPack.slug}/download/",
                                        ""),
                                    fileName = ver["name"].ToString().EndsWith(".jar")
                                        ? ver["name"].ToString()
                                        : ver["name"] + ".jar",
                                    type = (ReleaseType) Enum.Parse(typeof(ReleaseType), ver["type"].ToString().ToUpper())
                                };

                                selectedModInPack.otherVersions.Add(v);

                                if (v.type == selectedModInPack.selectedVersion.type)
                                    RunOnUIThread(() => modVersionSelect.Items.Add(v));
                            }
                        }

                        selectedModInPack.name = modData["title"].ToString();

                        RunOnUIThread(() =>
                        {
                            box.Items[box.SelectedIndex] = selectedModInPack;
                            releaseType.Enabled = true;
                            modVersionSelect.Enabled = true;

                            shouldUpdateVersion = true;
                            shouldHandleModsInPackClick = true;
                            //int pos = releaseType.Items.IndexOf(selectedModInPack.selectedVersion.type);
                            releaseType.SelectedItem = selectedModInPack.selectedVersion.type;

                            //pos = modVersionSelect.Items.IndexOf(selectedModInPack.selectedVersion);
                            modVersionSelect.SelectedItem = selectedModInPack.selectedVersion;

                            addModButton.Text = "Update";
                            removeModButton.Enabled = true;
                        });
                    }
                }
            });
        }

        private void RunOnUIThread(Action a)
        {
            Invoke((MethodInvoker) delegate { a(); });
        }

        private void SearchParamUpdated(object sender, EventArgs e)
        {
            if (modSearchBox.Text == "" || modSearchBox.Text == modSearchPlaceHolder) return;

            Task.Run(() =>
            {
                RunOnUIThread(() => modVersionSelect.Enabled = false);
                string url = "https://minecraft.curseforge.com/search?search=" + modSearchBox.Text;
                var doc = web.Load(url);

                RunOnUIThread(() => modSearchResults.Items.Clear());

                if (doc.DocumentNode.SelectSingleNode("//div[@class=\"alert alert-info\"]") is HtmlNode noResultsNode &&
                    noResultsNode.InnerText.Contains("No results for"))
                {
                    return;
                }

                var searchResults = doc.DocumentNode.SelectNodes("//tbody/tr[@class=\"results\"]");

                foreach (var searchResult in searchResults)
                {
                    var tableDataElements = searchResult.SelectNodes("td");
                    string modSlug = tableDataElements[0].SelectSingleNode("a").Attributes["href"].DeEntitizeValue
                        .Replace("/projects/", "");
                    modSlug = modSlug.Substring(0,
                        modSlug.Contains("?") ? modSlug.IndexOf("?", StringComparison.Ordinal) : modSlug.Length);

                    string modUrl = "https://minecraft.curseforge.com/projects/" + modSlug;

                    string name = WebUtility.HtmlDecode(tableDataElements[1]
                        .SelectSingleNode("div[@class=\"results-name\"]/a").InnerText);

                    ModSearchResult modSearchResult = new ModSearchResult
                    {
                        slug = modSlug,
                        url = modUrl,
                        name = name
                    };

                    RunOnUIThread(() => modSearchResults.Items.Add(modSearchResult));
                }
            });
        }

        private void OnReleaseTypeSelected(object sender, EventArgs e)
        {
            if (!shouldUpdateVersion) return;

            if (modSearchResults.SelectedItem is ModSearchResult selectedModSearchResult)
            {
                Task.Run(() =>
                {
                    while (selectedModSearchResult.versions == null || selectedModSearchResult.versions.Count == 0)
                    {
                        Thread.Sleep(200);
                    }

                    ReleaseType type = ReleaseType.ALPHA;

                    RunOnUIThread(() => type = (ReleaseType) releaseType.SelectedItem);

                    RunOnUIThread(() => modVersionSelect.Items.Clear());

                    foreach (ModVersion modVersion in selectedModSearchResult.versions.Where(v => v.type == type))
                    {
                        RunOnUIThread(() => modVersionSelect.Items.Add(modVersion));
                    }

                    RunOnUIThread(() => modVersionSelect.Enabled = true);
                });
            }
            else if (modsInPack.SelectedItem is ModInPack selectedModInPack)
            {
                Task.Run(() =>
                {
                    ReleaseType type = ReleaseType.ALPHA;

                    RunOnUIThread(() => type = (ReleaseType) releaseType.SelectedItem);

                    RunOnUIThread(() => modVersionSelect.Items.Clear());

                    foreach (ModVersion modVersion in selectedModInPack.otherVersions.Where(v => v.type == type))
                    {
                        RunOnUIThread(() => modVersionSelect.Items.Add(modVersion));
                    }

                    RunOnUIThread(() => modVersionSelect.Enabled = true);
                });
            }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            HttpWebRequest request =
                WebRequest.CreateHttp("https://launchermeta.mojang.com/mc/game/version_manifest.json");

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            using (Stream rS = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(rS ?? throw new Exception("Failed to get response stream")))
            {
                JObject gameVersions = JObject.Parse(reader.ReadToEnd());
                JArray versions = (JArray) gameVersions["versions"];
                foreach (var version in versions)
                {
                    if (version["type"].ToString() == "snapshot") continue;

                    mcVers.Items.Add(version["id"].ToString());
                }
            }

            request = WebRequest.CreateHttp(
                "http://files.minecraftforge.net/maven/net/minecraftforge/forge/maven-metadata.xml");

            using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
            using (Stream rS = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(rS ?? throw new Exception("Failed to get response stream")))
            {
                XElement meta = XElement.Parse(reader.ReadToEnd());

                foreach (string version in from seg in meta.Descendants("version")
                    select (string) seg)
                {
                    forgeVersions.Add(version);
                }
            }
        }

        private void OnMcVersSelected(object sender, EventArgs e)
        {
            List<string> versions = new List<string>();
            forgeVersion.Items.Clear();
            foreach (string fV in forgeVersions)
            {
                if (fV.StartsWith(mcVers.SelectedItem + "-"))
                {
                    versions.Add(fV);
                }
            }

            versions.Reverse();

            // Shut it, rider. This is exactly as it is supposed to be.
            // ReSharper disable once CoVariantArrayConversion
            forgeVersion.Items.AddRange(versions.ToArray());

            forgeVersion.Enabled = true;
        }

        private void OnForgeVersionSelected(object sender, EventArgs e)
        {
            modSearchBox.Enabled = true;
        }

        private void DoRemoveMod(object sender, EventArgs e)
        {
            if (modsInPack.SelectedItem is ModInPack selectedModInPack)
            {
                modsInPack.Items.Remove(selectedModInPack);

                if (modsInPack.Items.Count == 0)
                {
                    modName.Text = "No Mod Selected";
                    modDescription.Text = "Search for and select a mod...";
                    releaseType.Text = "(Release Type)";
                    modVersionSelect.Text = "(Version)";

                    releaseType.Enabled = false;
                    modVersionSelect.Enabled = false;
                    addModButton.Text = "Add";
                    removeModButton.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Select a mod in the list of mods in the pack on the right", "Advice");
            }
        }

        private void DoAddMod(object sender, EventArgs e)
        {
            if (modSearchResults.SelectedItem is ModSearchResult selectedSearchResult)
            {
                if (modsInPack.Items.Count == 1 && modsInPack.Items[0] is string) modsInPack.Items.Clear();

                if (modsInPack.Items.Cast<ModInPack>()
                        .FirstOrDefault(mod => mod.slug == selectedSearchResult.slug) is ModInPack mip &&
                    !Equals(mip, default(ModInPack)))
                {
                    modsInPack.Items.Remove(mip);
                }

                modsInPack.Items.Add(new ModInPack
                {
                    name = selectedSearchResult.name,
                    selectedVersion = (ModVersion) modVersionSelect.SelectedItem,
                    slug = selectedSearchResult.slug,
                    url = selectedSearchResult.url
                });

                HtmlDocument doc = web.Load(selectedSearchResult.url + "/files/" +
                                            ((ModVersion) modVersionSelect.SelectedItem).fileId);

                var related = doc.DocumentNode.SelectSingleNode(@"//section[@class=""details-related-projects""]");

                if (related == null) return;

                bool next = false;
                foreach (var child in related.ChildNodes)
                {
                    if (child.Name == "h5")
                        next = true;
                    else if (next && child.Name == "ul")
                    {
                        next = false;
                        selectedSearchResult.dependencies = new List<ModSearchResult>();

                        foreach (var dep in child.ChildNodes)
                        {
                            string slug = dep.SelectSingleNode("//li[@class=\"project-tag\"]/a").Attributes["href"]
                                .DeEntitizeValue.Replace("/projects/", "");

                            ModSearchResult dependency = new ModSearchResult
                            {
                                slug = slug,
                                name = dep.SelectSingleNode("//div[@class=\"project-tag-name overflow-tip\"]/span")
                                    .InnerText,
                                url = "https://minecraft.curseforge.com/projects/" + slug
                            };

                            if (!selectedSearchResult.dependencies.Contains(dependency))
                                selectedSearchResult.dependencies.Add(dependency);
                        }
                    }
                }

                if (selectedSearchResult.dependencies.Count > 1)
                {
                    string text =
                        "This mod depends on multiple dependencies - here they are, you'll have to browse for them on your own: [";

                    selectedSearchResult.dependencies.ForEach(dep => text += dep.name + ", ");

                    text = text.Substring(0, text.Length - 2) + "]";

                    MessageBox.Show(text, "Multiple Dependencies");
                }
                else if (selectedSearchResult.dependencies.Count == 1)
                {
                    DialogResult result = MessageBox.Show(
                        "This mod depends on \"" + selectedSearchResult.dependencies[0].name +
                        "\". Would you like to search for it now?",
                        "Mod Dependency", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        modSearchResults.Items.Clear();
                        modSearchResults.Items.Add(selectedSearchResult.dependencies[0]);
                        modSearchResults.SelectedIndex = 0;
                        OnSearchItemClicked(modSearchResults, EventArgs.Empty);
                    }
                }
            }
            else if (modsInPack.SelectedItem is ModInPack selectedModInPack)
            {
                shouldHandleModsInPackClick = false;
                int index = modsInPack.SelectedIndex;

                modsInPack.Items[index] = new ModInPack
                {
                    name = selectedModInPack.name,
                    selectedVersion = (ModVersion) modVersionSelect.SelectedItem,
                    slug = selectedModInPack.slug,
                    url = selectedModInPack.url
                };
                shouldHandleModsInPackClick = true;
            }
        }

        private void SavePack(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                OverwritePrompt = true,
                AddExtension = true,
                DefaultExt = ".sbp",
                FileName = packName.Text + ".sbp",
                Filter = "SamboyPack File (*.sbp)|*.sbp|SQL File For Direct Insertion (*.sql)|*.sql"
            };
            DialogResult result = dialog.ShowDialog(this);

            if (result != DialogResult.OK) return;

            if (File.Exists(dialog.FileName)) File.Delete(dialog.FileName);

            switch (dialog.FilterIndex)
            {
                case 1:
                    //Save as SBP File
                    using (Stream s = dialog.OpenFile())
                    using (StreamWriter sw = new StreamWriter(s))
                    {
                        sw.WriteLine(packName.Text);
                        sw.WriteLine(packVersion.Text);
                        sw.WriteLine(packDescription.Text.Replace("\r\n", "\\n"));
                        sw.WriteLine(mcVers.SelectedItem);
                        sw.WriteLine(forgeVersion.SelectedItem);
                        foreach (var m in modsInPack.Items)
                        {
                            if (m is ModInPack mod)
                            {
                                sw.WriteLine(mod.slug + "|" + mod.selectedVersion.fileId);
                            }
                        }
                    }

                    break;
                case 2:
                    //Save as SQL
                    //INSERT INTO `PACKS` (`PACK_NAME`, `VERSION`, `DESCRIPTION`, `GAME_VERSION`, `FORGE_VERSION`, `MODS`) VALUES
                    //(NAME, VERS, DESC, MCVER, FORGEVER, MODS)

                    string query =
                        "INSERT INTO `PACKS` (`PACK_NAME`, `VERSION`, `DESCRIPTION`, `GAME_VERSION`, `FORGE_VERSION`, `MODS`) VALUES('";
                    query += packName.Text.Replace("'", "") + "', '";
                    query += packVersion.Text.Replace("'", "") + "', '";
                    query += packDescription.Text.Replace("\r\n", "\\\\n").Replace("'", "") + "', '";
                    query += mcVers.Text + "', '";
                    query += forgeVersion.Text.Replace(mcVers.Text + "-", "").Replace("-" + mcVers.Text, "") + "', '";
                    if (modsInPack.Items.Count == 0 || modsInPack.Items.Count == 1 && modsInPack.Items[0] is string)
                        query += "');";
                    else
                    {
                        bool first = true;
                        foreach (ModInPack mod in modsInPack.Items)
                        {
                            query += (first ? "" : ",") + mod.url + "/files/" + mod.selectedVersion.fileId;
                            first = false;
                        }

                        query += "');";
                    }

                    File.WriteAllText(dialog.FileName, query);
                    break;
            }
        }

        private void LoadPack(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                DefaultExt = ".sbp",
                Filter = "SamboyPack File (*.sbp)|*.sbp|Exported SQL (*.sql)|*.sql"
            };
            DialogResult result = dialog.ShowDialog(this);

            if (result != DialogResult.OK) return;

            openPackButton.Enabled = false;
            savePackButton.Enabled = false;
            mcVers.Enabled = false;
            modSearchBox.Enabled = false;
            addModButton.Enabled = false;
            UseWaitCursor = true;

            //Again, 1-indexed crap
            switch (dialog.FilterIndex)
            {
                case 1:
                    using (Stream s = dialog.OpenFile())
                    using (StreamReader sw = new StreamReader(s))
                    {
                        packName.Text = sw.ReadLine();
                        packVersion.Text = sw.ReadLine();
                        packDescription.Text = sw.ReadLine()?.Replace("\\n", "\r\n");
                        mcVers.SelectedItem = sw.ReadLine();
                        forgeVersion.SelectedItem = sw.ReadLine();
                        modsInPack.Items.Clear();
                        string line;
                        while ((line = sw.ReadLine()) != null)
                        {
                            string[] split = line.Split('|');
                            modsInPack.Items.Add(new ModInPack
                            {
                                slug = split[0],
                                url = "https://minecraft.curseforge.com/projects/" + split[0],
                                selectedVersion = new ModVersion
                                {
                                    fileId = split[1]
                                },
                                otherVersions = new List<ModVersion>()
                            });
                        }
                    }

                    break;
                case 2:
                    string query = File.ReadAllText(dialog.FileName);
                    int start = query.IndexOf("INSERT INTO", StringComparison.Ordinal);
                    int end = query.Substring(start).IndexOf(");", StringComparison.Ordinal) + 2;

                    query = query.Substring(start, end);

                    start = query.ToUpper().IndexOf("VALUES", StringComparison.Ordinal) + 6;
                    string values = query.Substring(start,
                        query.IndexOf(");", StringComparison.Ordinal) + 1 - start).Trim();

                    //Remove brackets
                    values = values.Substring(1, values.Length - 2);
                    packDescription.Text = values;

                    Match match = Regex.Match(values,
                        @"\d+,\s*'([\w\d\s\.]+)',\s*'([0-9\.]+)',\s*'([\w\s\d\.,]+)',\s*'([\d\.]+)',\s*'([\d\.]+)',\s*('(https:\/\/minecraft\.curseforge\.com\/projects\/[a-zA-Z\-\d]+\/files\/[\d]+,*)*')");

                    packName.Text = match.Groups[1].Value;
                    packVersion.Text = match.Groups[2].Value;
                    packDescription.Text = match.Groups[3].Value;
                    mcVers.SelectedItem = match.Groups[4].Value;

                    string forgeVers = match.Groups[5].Value;
                    if (!forgeVersions.Contains(forgeVers))
                    {
                        forgeVers = mcVers + "-" + forgeVers;
                    }

                    forgeVersion.SelectedItem = forgeVers;

                    string modListing = match.Groups[6].Value.Replace("'", "");
                    string[] modStrings = modListing.Split(',');

                    modsInPack.Items.Clear();
                    foreach (var modString in modStrings)
                    {
                        Match m = Regex.Match(modString,
                            @"https:\/\/minecraft\.curseforge\.com\/projects\/([a-zA-Z\-\d]+)\/files\/([\d]+)");
                        modsInPack.Items.Add(new ModInPack
                        {
                            slug = m.Groups[1].Value,
                            url = "https://minecraft.curseforge.com/projects/" + m.Groups[1].Value,
                            selectedVersion = new ModVersion
                            {
                                fileId = m.Groups[2].Value
                            },
                            otherVersions = new List<ModVersion>()
                        });
                    }

                    break;
                default:
                    MessageBox.Show("Open: Invalid filter index " + dialog.FilterIndex);
                    Environment.Exit(-1);
                    break;
            }

            MessageBox.Show("Please wait while mod information is retreived...");

            List<ModInPack> newModsInPack = new List<ModInPack>();

            Task.Run(() =>
            {
                string selectedMCVers = null;

                RunOnUIThread(() => selectedMCVers = (string) mcVers.SelectedItem);

                object[] items = new object[modsInPack.Items.Count];
                modsInPack.Items.CopyTo(items, 0);

                List<ModInPack> notDone = items.OfType<ModInPack>().ToList();
                foreach (var m in items)
                {
                    if (!(m is ModInPack mod)) continue;

                    HttpWebRequest request =
                        WebRequest.CreateHttp($"https://api.cfwidget.com/minecraft/mc-mods/{mod.slug}");


                    using (HttpWebResponse response = GetResponseNoException(request))
                    using (Stream rS = response.GetResponseStream())
                    using (StreamReader reader =
                        new StreamReader(rS ?? throw new Exception("Failed to get response stream")))
                    {
                        JObject modData = JObject.Parse(reader.ReadToEnd());
                        if (modData.ContainsKey("error") && modData["error"].ToString() == "in_queue")
                        {
                            RunOnUIThread(() =>
                            {
                                MessageBox.Show(
                                    "Mod in queue. Please try again in 10-30 seconds.",
                                    "In Queue...");
                            });
                        }
                        else if (!modData.ContainsKey("error"))
                        {
                            JArray rawVersions = (JArray) modData["files"];

                            foreach (var ver in rawVersions)
                            {
                                if (((JArray) ver["versions"]).All(mcVer => mcVer.ToString() != selectedMCVers)) continue;
                                ModVersion v = new ModVersion
                                {
                                    fileId = ver["url"].ToString()
                                        .Replace($"https://www.curseforge.com/minecraft/mc-mods/{mod.slug}/download/", ""),
                                    fileName = ver["name"].ToString().EndsWith(".jar")
                                        ? ver["name"].ToString()
                                        : ver["name"] + ".jar",
                                    type = (ReleaseType) Enum.Parse(typeof(ReleaseType), ver["type"].ToString().ToUpper())
                                };

                                mod.otherVersions.Add(v);

                                if (v.fileId == mod.selectedVersion.fileId) mod.selectedVersion = v;
                            }

                            mod.name = modData["title"].ToString();
                        }
                    }

                    newModsInPack.Add(mod);
                    notDone.Remove(mod);

                    RunOnUIThread(() =>
                    {
                        modsInPack.Items.Clear();
                        foreach (ModInPack m2 in newModsInPack)
                        {
                            modsInPack.Items.Add(m2);
                        }

                        foreach (ModInPack m2 in notDone)
                        {
                            modsInPack.Items.Add(m2);
                        }
                    });
                }

                RunOnUIThread(() =>
                {
                    modsInPack.Items.Clear();
                    foreach (ModInPack m in newModsInPack)
                    {
                        modsInPack.Items.Add(m);
                    }

                    openPackButton.Enabled = true;
                    savePackButton.Enabled = true;
                    mcVers.Enabled = true;
                    modSearchBox.Enabled = true;
                    addModButton.Enabled = true;
                    UseWaitCursor = false;
                });
            });
        }

        private static HttpWebResponse GetResponseNoException(HttpWebRequest req)
        {
            try
            {
                return (HttpWebResponse) req.GetResponse();
            }
            catch (WebException we)
            {
                if (!(we.Response is HttpWebResponse resp))
                    throw;
                return resp;
            }
        }
    }
}